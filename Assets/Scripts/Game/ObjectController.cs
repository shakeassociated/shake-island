﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectController : MonoBehaviour
{
    public int id;
    public string prettyName;

    private GameController gameController;

    public GameController GameController
    {
        get
        {
            if (gameController != null)
                return gameController;
            return FindObjectOfType<GameController>();
        }
        set { gameController = value; }
    }

    private void Start()
    {
        AssignCorrespondingTextures();
    }

    public void Init(GameController gameController)
    {
        this.gameController = gameController;
    }

    public void IncreaseID()
    {
        id = GameController.currentObjectIndex;
        GameController.currentObjectIndex++;
    }

    /// <summary>
    /// otherObject must be from the same prefab
    /// </summary>
    /// <param name="otherObject"></param>
    public void CopyTexturesOf(GameObject otherObject)
    {
        /* Assigning ids of materials, if they exist */
        var newTextureCtrl = otherObject.GetComponent<TextureController>();
        var textureCtrl = GetComponent<TextureController>();

        if (textureCtrl != null && newTextureCtrl != null)
            textureCtrl.currentMaterialIndex = newTextureCtrl.currentMaterialIndex;

        CopyTexturesOfRec(otherObject, transform);

        /* Re-assigning textures */
        AssignCorrespondingTextures();
    }


    /// <summary>
    /// otherObject must be from the same prefab
    /// </summary>
    /// <param name="otherObject"></param>
    public void CopyTexturesOfRec(GameObject otherObject, Transform o)
    {
        foreach (Transform child in o.transform)
        {
            string currName = child.gameObject.name;
            GameObject newObjectChild = otherObject.transform.FindDeepChild(currName).gameObject;

            var newObjectChildTextureCtrl = newObjectChild.GetComponent<TextureController>();
            var childTextureCtrl = child.gameObject.GetComponent<TextureController>();

            if (newObjectChildTextureCtrl != null && childTextureCtrl != null)
                childTextureCtrl.currentMaterialIndex = newObjectChildTextureCtrl.currentMaterialIndex;

            CopyTexturesOfRec(otherObject, child.transform);
        }
    }

    public void AssignCorrespondingTextures()
    {
        var textureCtrl = GetComponent<TextureController>();
        if (textureCtrl != null)
        {
            Renderer oRenderer = GetComponent<Renderer>();
            List<Material> oMaterials = textureCtrl.materials;
            oRenderer.material = oMaterials[textureCtrl.currentMaterialIndex];
        }
        AssignCorrespondingTexture(gameObject);
    }

    private void AssignCorrespondingTexture(GameObject o)
    {
        foreach (Transform child in o.transform)
        {
            var textureCtrl = child.GetComponent<TextureController>();
            if (textureCtrl != null)
            {
                Renderer oRenderer = child.GetComponent<Renderer>();
                List<Material> oMaterials = textureCtrl.materials;
                oRenderer.material = oMaterials[textureCtrl.currentMaterialIndex];
            }

            AssignCorrespondingTexture(child.gameObject);
        }
    }

    /*
     * Same id
     * 
     * If differents id :
     * prefab name
     * texturesControllers
     */
    public bool IsTheSameAs(ObjectController other)
    {
        if (id == other.id)
            return true;

        if (name != other.name)
            return false;
        
        return SameTexturesOf(other.transform.gameObject);
    }

    public bool SameTexturesOf(GameObject otherObject)
    {
        var newTextureCtrl = otherObject.GetComponent<TextureController>();
        var textureCtrl = GetComponent<TextureController>();

        if (textureCtrl == null && newTextureCtrl != null)
            return false;
        else if (textureCtrl != null && newTextureCtrl == null)
            return false;
        else if (textureCtrl == null && newTextureCtrl == null)
            return SameTexturesOfRec(otherObject, transform);
        else
        {
            if (textureCtrl.currentMaterialIndex != newTextureCtrl.currentMaterialIndex)
                return false;

            return SameTexturesOfRec(otherObject, transform);
        }
    }

    public bool SameTexturesOfRec(GameObject otherObject, Transform o)
    {
        bool res = true;
        foreach (Transform child in o.transform)
        {
            string currName = child.gameObject.name;

            var tmp = otherObject.transform.FindDeepChild(currName);
            if (tmp == null)
                return false;
            GameObject newObjectChild = tmp.gameObject;

            var newObjectChildTextureCtrl = newObjectChild.GetComponent<TextureController>();
            var childTextureCtrl = child.gameObject.GetComponent<TextureController>();

            if (newObjectChildTextureCtrl == null && childTextureCtrl != null)
                return false;
            else if (newObjectChildTextureCtrl != null && childTextureCtrl == null)
                return false;
            else if (newObjectChildTextureCtrl == null && childTextureCtrl == null)
                res &= SameTexturesOfRec(otherObject, child.transform);
            else
            {
                res &= (childTextureCtrl.currentMaterialIndex == newObjectChildTextureCtrl.currentMaterialIndex)
                    && SameTexturesOfRec(otherObject, child.transform);
            }
        }
        return res;
    }
}
