﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterBottleController : MonoBehaviour {
    bool shouldBeCold;
    TextureController tc;
    MeshRenderer contentRenderer;
    GameObject contentGameObject;

    private void Awake()
    {
        contentRenderer = transform.Find("Content").GetComponent<MeshRenderer>();
        contentGameObject = transform.Find("Content").gameObject;
        tc = transform.Find("Content").GetComponent<TextureController>();
    }

    public void SetInitialValue()
    {
        shouldBeCold = tc.currentMaterialIndex == 0;
    }

    public void Shake()
    {
        contentGameObject.SetActive(false);
    }

    // if !cold : very cold -> change content's material
    public void Fill(bool cold)
    {
        tc.currentMaterialIndex = cold ? 0 : 1;
        contentRenderer.material = tc.materials[tc.currentMaterialIndex];

        contentGameObject.SetActive(true);
    }

    public bool isValidForScore()
    {
        return contentGameObject.activeSelf
            && ((shouldBeCold && tc.currentMaterialIndex == 0) || (!shouldBeCold && tc.currentMaterialIndex == 1));
    }
}
