﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterFillerColdButtonController : InteractionButtonController
{
    PlayerController playerController;
    HUDController hudController;

    private void Awake()
    {
        playerController = FindObjectOfType<PlayerController>();
    }

    public override void Start()
    {
        base.Start();
        hudController = transform.parent.parent.GetComponent<ObjectController>().GameController.hudController;
    }

    public override void DoAction()
    {
        if (timerReclick.IsOver())
        {
            if (playerController.collected != null && playerController.collected.name == "Bottle")
            {
                var waterBottleController = playerController.collected.GetComponent<WaterBottleController>();
                waterBottleController.Fill(true);
                hudController.DisplayCollectedObject(waterBottleController.gameObject);
            }
        }
        base.DoAction();
    }
}
