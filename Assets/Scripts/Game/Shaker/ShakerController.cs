﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakerController : MonoBehaviour {
    float timerGameSeconds, lerpPosition, animatorSpeed, lerpAnimation;
    public Transform startPosT, endPosT;
    Vector3 startPos, endPos;
    Animator ac;

    private void Awake()
    {
        ac = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        lerpPosition += Time.deltaTime / timerGameSeconds;
        transform.position = Vector3.Lerp(startPos, endPos, lerpPosition);

        lerpAnimation += Time.deltaTime / timerGameSeconds;
        animatorSpeed = Mathf.Lerp(1f, 10f, lerpAnimation);
        ac.speed = animatorSpeed;
    }

    public void Restart(GameController gameController)
    {
        timerGameSeconds = gameController.timerGameSeconds;
        startPos = startPosT.position;
        endPos = endPosT.position;
        lerpPosition = 0;

        animatorSpeed = 1f;
        ac.speed = animatorSpeed;
        lerpAnimation = 0;
    }
}
