﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;

public class NPCButtonFinishController : InteractionButtonController
{
    FirstPersonController fpsController;
    HUDController hudController;
    NPCController npcController;

    public override void Start()
    {
        base.Start();
        fpsController = FindObjectOfType<FirstPersonController>();
        floating = true;
        hudController = GameController.hudController;
        npcController = transform.parent.parent.GetComponent<NPCController>();
    }

    public override void DoAction()
    {
        if (GameController.gameType == GameController.GameType.GAME && timerReclick.IsOver())
        {
            ScoreController scoreCtrl = GameController.scoreController;
            var score = scoreCtrl.Analyse(true);

            if (GameController.isTutorial)
            {
                if (score.notePercentage != 100)
                {
                    AudioSource audioSource = gameObject.AddComponent<AudioSource>();
                    audioSource.clip = (AudioClip)Resources.Load("Sounds/NPCs/tutorial_error");

                    hudController.MakeNPCTalk(
                        npcController,
                        "Tous les objets de la pièce n'ont pas été placés correctement !",
                        audioSource);
                }
                else
                    GameController.mapController.LoadNextTutorial(GameController.gameType == GameController.GameType.EDITOR);
            }
            else
            {
                gameController.hudController.EnableCursorAndFreezeFPSWithoutTimeScale(true);
                LevelProperties.score = score;
                MenuProperties.isPlayer = true;
                SceneManager.LoadScene("Menu");
            }
        }
        base.DoAction();
    }
}
