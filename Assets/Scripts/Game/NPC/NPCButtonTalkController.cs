﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class NPCButtonTalkController : InteractionButtonController
{
    NPCController npcController;
    public string fullText;
    HUDController hudController;
    new AudioSource audio;

    public override void Start()
    {
        base.Start();
        if (gameController)
            hudController = GameController.hudController;
        floating = true;

        npcController = transform.parent.parent.GetComponent<NPCController>();
        audio = null;
        var audiosources = npcController.GetComponents<AudioSource>();
        if (LevelProperties.levelTypeToLoad == LevelProperties.LevelType.RESOURCE && !GameController.isTutorial)
        {
            foreach (var a in audiosources)
            {
                if (a.clip.name == "message")
                    audio = a;
            }
        }
    }

    public override void DoAction()
    {
        if (timerReclick.IsOver())
        {
            if (!hudController)
                hudController = GameController.hudController;

            if (hudController)
                hudController.MakeNPCTalk(npcController, fullText, audio);
        }
        base.DoAction();
    }
}
