﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCRandomController : MonoBehaviour {
    public enum RandomType { IDLE, TALK, WALK };
    public RandomType randomType;
    Animator random_ac;

    private void Awake()
    {
        random_ac = GetComponent<Animator>();
    }

    void Start () {
        random_ac.Play(TypeToString());
	}

    string TypeToString()
    {
        switch (randomType)
        {
            case RandomType.IDLE:
                return "Idle";
            case RandomType.TALK:
                return "Talk";
            case RandomType.WALK:
                return "Walk";
        }
        return "Idle";
    }
}
