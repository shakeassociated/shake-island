﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCController : MonoBehaviour
{
    public bool buttonFinish, buttonTalk;
    bool talkToMe;
    public NPCButtonFinishController npcButtonFinishController;
    public NPCButtonTalkController npcButtonTalkController;
    private GameObject exclamationMark;
    Animator animator;
    bool isEditor;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        Transform tempTransform;
        if ((tempTransform = transform.Find("ExclamationMark")) != null)
            exclamationMark = transform.Find("ExclamationMark").gameObject;
        else
            exclamationMark = null;

        npcButtonFinishController = GetComponentInChildren<NPCButtonFinishController>();
        npcButtonTalkController = GetComponentInChildren<NPCButtonTalkController>();
    }

    private void Start()
    {
        isEditor = !CompareTag("npc");
        SetFinishActive(isEditor ? buttonFinish : false);
        SetTalkActive(isEditor ? buttonTalk : npcButtonTalkController.fullText != "");

        // Activates exclamation if button talk
        ActivateExclamationMark(!isEditor && buttonTalk);
    }

    public void SetFinishActive(bool val)
    {
        npcButtonFinishController.gameObject.SetActive(val);
    }

    public void SetTalkActive(bool val)
    {
        npcButtonTalkController.gameObject.SetActive(val);
    }

    public void EnableTalkingAnimation(bool talk)
    {
        animator.SetBool("Talk", talk);
        if (talkToMe)
            ActivateExclamationMark(false);
    }

    private void ActivateExclamationMark(bool val)
    {
        if (exclamationMark != null)
        {
            exclamationMark.SetActive(val);
            talkToMe = val;
        }
    }

    public void DisableTalkToMe()
    {
        ActivateExclamationMark(false);
    }

    public void EndShake()
    {
        if (buttonFinish)
            SetFinishActive(true);

        ActivateExclamationMark(false);
        SetTalkActive(false);
    }
}
