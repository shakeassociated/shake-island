﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCRandomColor : MonoBehaviour {
    List<Color> skins, hairs, eyes;
	void Start () {
        InitColors();

        var materials = transform.FindDeepChild("Body").gameObject.GetComponent<SkinnedMeshRenderer>().materials;
        foreach (var mat in materials)
            mat.color = NewColorFromMaterial(mat);
    }

    private Color hexColor(float r, float g, float b)
    {
        return new Color(r / 255, g / 255, b / 255);
    }

    private void InitColors()
    {
        InitSkinsColor();
        InitHairsColor();
        InitEyesColor();
    }

    private void InitSkinsColor()
    {
        skins = new List<Color>();
        skins.Add(hexColor(200, 101, 60));
        skins.Add(hexColor(137, 67, 38));
        skins.Add(hexColor(255, 147, 101));
        skins.Add(hexColor(255, 198, 174));
    }

    private void InitHairsColor()
    {
        hairs = new List<Color>();
        hairs.Add(hexColor(6, 5, 6));
        hairs.Add(hexColor(176, 210, 54));
        hairs.Add(hexColor(53, 68, 0));
        hairs.Add(hexColor(188, 74, 48));
        hairs.Add(hexColor(219, 219, 219));
    }

    private void InitEyesColor()
    {
        eyes = new List<Color>();
        eyes.Add(hexColor(0, 6, 204));
        eyes.Add(hexColor(154, 101, 81));
        eyes.Add(hexColor(0, 0, 0));
        eyes.Add(hexColor(81, 186, 168));
        eyes.Add(hexColor(221, 0, 0));
    }

    private Color NewColorFromMaterial(Material mat)
    {
        if (MaterialIs(mat, "skin"))
            return skins[Random.Range(0, skins.Count)];
        if (MaterialIs(mat, "hair"))
            return hairs[Random.Range(0, hairs.Count)];
        if (MaterialIs(mat, "eyes"))
            return eyes[Random.Range(0, eyes.Count)];
        else if (CanChangeMatColor(mat))
            return new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));

        return mat.color;
    }

    private bool CanChangeMatColor(Material mat)
    {
        return !MaterialIs(mat, "mouth") && !MaterialIs(mat, "White");
    }

    private bool MaterialIs(Material mat, string name)
    {
        return mat.name == name + " (Instance)";
    }
}
