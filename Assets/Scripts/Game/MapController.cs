﻿using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MapController : MonoBehaviour
{
    GameController gameController;
    MapsController mapsController;
    bool refreshingMaps;
    public bool RefreshingMaps
    {
        get { return refreshingMaps; }
    }
    public MapInfo mapInfo;
    public string mapName;
    private Map currDownloadedMap;
    public Map CurrDownloadedMap
    {
        get { return currDownloadedMap; }
    }

    private void Awake()
    {
        gameController = GetComponent<GameController>();
        mapsController = FindObjectOfType<MapsController>();
    }

    public void Init()
    {
        currDownloadedMap = null;
        mapInfo = new MapInfo();
        mapInfo.Init();
        mapName = LevelProperties.levelToLoad;
    }

    private List<ObjectTexture> GetObjectsTexture(GameObject o)
    {
        List<ObjectTexture> res = new List<ObjectTexture>();

        var textureCtrl = o.GetComponent<TextureController>();
        if (textureCtrl != null)
            res.Add(new ObjectTexture("parent", textureCtrl.currentMaterialIndex));

        GetObjectsTextureRef(o, res);

        return res;
    }

    private void GetObjectsTextureRef(GameObject o, List<ObjectTexture> res)
    {
        foreach (Transform child in o.transform)
        {
            var textureCtrl = child.GetComponent<TextureController>();
            if (textureCtrl != null)
                res.Add(new ObjectTexture(child.name, textureCtrl.currentMaterialIndex));

            GetObjectsTextureRef(child.gameObject, res);
        }
    }

    private ObjectTexture GetObjectTextures(GameObject o)
    {
        var textureCtrl = o.GetComponent<TextureController>();
        return textureCtrl != null ? new ObjectTexture(o.name, textureCtrl.currentMaterialIndex) : null;
    }
    
    public void SaveMap(bool editor)
    {
        mapInfo.Clear();

        if (editor)
            mapName = LevelEditorProperties.levelName;

        mapInfo.SetName(mapName);
        SaveTutorial();
        SaveMapObjects();
        SavePlayer(editor);
        SaveShakeCamera();
        SaveNPCs();

        print("saving with name : " + mapInfo.mapName);

        string jsonGenerated = mapInfo.Serialize();
        var filePath = Application.persistentDataPath + "/Maps/Local/" + mapInfo.mapName + ".txt";

        FileInfo file = new FileInfo(filePath);
        file.Directory.Create();

        File.WriteAllText(filePath, jsonGenerated);

        Debug.Log("Saved map " + "\"" + mapInfo.mapName + "\"");
    }

    private void SaveTutorial()
    {
        mapInfo.isTutorial = gameController.isTutorial;
        mapInfo.tutorialData = gameController.tutorialData;
    }

    private void SavePlayer(bool editor)
    {
        GameObject player = GameObject.Find("Player");
        if (editor)
            mapInfo.SetPlayerInfo(player.GetComponent<PlayerController>().OriginPos);
        mapInfo.SetPlayerInfo(player.transform.position);
    }

    private void SaveShakeCamera()
    {
        GameObject shakeCamera = GameObject.Find("CameraWhenShakeParent");
        mapInfo.SetShakeCameraInfo(shakeCamera.transform);
    }

    /* Saves into mapInfo :
     * Not inner slots
     * Editor Furnitures (+ inner slots) + Perfs
     * Editor NPCs
     * Collectables
     * Random Objects (slotless furnitures, walls, etc.)
     */
    private void SaveMapObjects()
    {
        ObjectController[] objectsControllers = FindObjectsOfType<ObjectController>();
        foreach (ObjectController o in objectsControllers)
        {
            GameObject mapObject = o.gameObject;
            List<ObjectTexture> objectsTexture = GetObjectsTexture(mapObject);

            SlotController slotController;
            CollectableController colController;
            
            // Slot (Not inner)
            if ((slotController = mapObject.GetComponent<SlotController>()) != null)
            {
                // Don't save inner slots because they are saved within furnitures
                if (!slotController.IsAnInnerSlot)
                {
                    MapSlot mapSlot = new MapSlot(
                        o.id,
                        mapObject.name,
                        mapObject.transform,
                        objectsTexture,
                        slotController);

                    mapInfo.AddMapSlot(mapSlot);
                }
            }
            else if ((colController = mapObject.GetComponent<CollectableController>()) != null)
            {
                // EDITOR Furniture
                if (colController.IsEditorFurnitureType())
                {
                    // Perf
                    WaterController waterController;
                    if ((waterController = mapObject.GetComponentInChildren<WaterController>()) != null)
                    {
                        MapPerf mapPerf = new MapPerf(o.id, mapObject.transform, waterController.WaterLevel);
                        mapInfo.AddPerf(mapPerf);
                    }
                    else
                    {
                        // Fills innerSlots
                        List<MapSlot> innerSlots = new List<MapSlot>();
                        var slotControllers = o.GetComponentsInChildren<SlotController>();
                        foreach (var sc in slotControllers)
                        {
                            var go = sc.gameObject;

                            innerSlots.Add(new MapSlot(
                                go.GetComponent<ObjectController>().id,
                                go.name,
                                go.transform,
                                objectsTexture,
                                sc));
                        }

                        MapFurniture mapFurniture = new MapFurniture(
                            o.id,
                            mapObject.name,
                            mapObject.transform,
                            objectsTexture,
                            innerSlots);

                        mapInfo.AddMapFurniture(mapFurniture);
                    }
                }
                // EDITOR NPC
                else if (colController.collectableType == CollectableType.NPC_EDITOR)
                {
                    var npcController = o.GetComponent<NPCController>();

                    string talkText = "";
                    if (npcController.buttonTalk)
                        talkText = npcController.npcButtonTalkController.fullText;

                    MapNPC mapNPC = new MapNPC(
                        o.id,
                        mapObject.name,
                        mapObject.transform,
                        npcController.buttonFinish,
                        talkText,
                        true);

                    mapInfo.AddNPC(mapNPC);
                }
                // Collectable
                else
                {
                    MapCollectable mapCollectable = new MapCollectable(
                        o.id,
                        mapObject.name,
                        mapObject.transform,
                        objectsTexture,
                        colController);

                    mapInfo.AddMapCollectable(mapCollectable);
                }
            }
            // Slotless furnitures and random objects (like walls, etc.)
            else
            {
                MapObject mo = new MapObject(
                    o.id,
                    mapObject.name,
                    mapObject.transform,
                    objectsTexture);

                mapInfo.AddMapObject(mo);
            }
        }
    }

    // Saves non-Editor NPCs
    private void SaveNPCs()
    {
        var npcs = GameObject.FindGameObjectsWithTag("npc");

        foreach (var npc in npcs)
        {
            var npcController = npc.GetComponent<NPCController>();
            string talkText = "";
            if (npcController.npcButtonTalkController)
                talkText = npcController.npcButtonTalkController.fullText;

            var objCtrl = npc.GetComponent<ObjectController>();

            MapNPC mapNPC = new MapNPC(
                objCtrl.id,
                npc.name,
                npc.transform,
                npcController.buttonFinish,
                talkText,
                false);
            mapInfo.AddNPC(mapNPC);
        }
    }

    public void ClearMap(bool restart)
    {
        foreach (ObjectController o in gameController.ListObjectCtrl)
        {
            bool isNotNPC = o.GetComponent<NPCController>() == null;
            if (isNotNPC || restart)
                DestroyImmediate(o.gameObject);
        }
        

        foreach (GameObject o in gameController.ListPerfs)
            DestroyImmediate(o);

        if (restart)
        {
            foreach (GameObject o in gameController.ListNPC)
                DestroyImmediate(o);

            gameController.Restart();
        }
    }

    public void LoadNextTutorial(bool editor)
    {
        int currTuto = (int)Char.GetNumericValue(mapName[mapName.Length - 1]);

        if (currTuto == gameController.nbTutorials)
        {
            gameController.hudController.EnableCursorAndFreezeFPSWithoutTimeScale(true);
            var score = gameController.scoreController.Analyse(true);
            LevelProperties.score = score;
            MenuProperties.isPlayer = true;
            SceneManager.LoadScene("Menu");
        }
        else
        {
            mapName = "tuto" + (currTuto + 1);
            LevelProperties.levelToLoad = mapName;
            LevelProperties.levelTypeToLoad = LevelProperties.LevelType.RESOURCE;
            LoadMap(editor);
        }
    }

    public void LoadMap(bool editor)
    {
        /* DESTROY */
        ClearMap(true);

        var levelType = LevelProperties.levelTypeToLoad;
        var jsonLoaded = "";
        if (levelType == LevelProperties.LevelType.RESOURCE)
        {
            var resource = Resources.Load("Levels/" + mapName) as TextAsset;
            jsonLoaded = resource.text;

            print("Loaded map \"" + mapName + "\" from resources");
        }
        else if (levelType == LevelProperties.LevelType.DOWNLOADED)
        {
            var jsonMap = File.ReadAllText(Application.persistentDataPath + "/Maps/Downloaded/" + LevelProperties.levelToLoad + ".txt");
            Map map = JsonUtility.FromJson<Map>(jsonMap);
            jsonLoaded = map.json;
            currDownloadedMap = map;
            print("Loaded map \"" + mapName + "\" from downloaded maps (by " + map.user.pseudo + ")");
        }
        else
        {
            jsonLoaded = File.ReadAllText(Application.persistentDataPath + "/Maps/Local/" + mapName + ".txt");
            print("Loaded map \"" + mapName + "\" from local persitent data");
        }

        mapInfo = JsonUtility.FromJson<MapInfo>(jsonLoaded);
        
        LoadPlayer();
        LoadShakeCamera();
        LoadMapObjects();
        LoadMapSlots();
        LoadMapFurnitures(editor);
        LoadMapCollectables();
        LoadMapNPCs(editor);
        LoadMapPerfs(editor);
        LoadTutorialInfo();
    }

    public void ReloadMapContent(bool editor)
    {
        /* DESTROY */
        ClearMap(false);

        var levelType = LevelProperties.levelTypeToLoad;
        var jsonLoaded = "";
        if (levelType == LevelProperties.LevelType.RESOURCE)
        {
            var resource = Resources.Load("Levels/" + mapName) as TextAsset;
            jsonLoaded = resource.text;

            print("Loaded map \"" + mapName + "\" from resources");
        }
        else if (levelType == LevelProperties.LevelType.DOWNLOADED)
        {
            var jsonMap = File.ReadAllText(Application.persistentDataPath + "/Maps/Downloaded/" + LevelProperties.levelToLoad + ".txt");
            Map map = JsonUtility.FromJson<Map>(jsonMap);
            jsonLoaded = map.json;
            currDownloadedMap = map;
            print("Loaded map \"" + mapName + "\" from downloaded maps (by " + map.user.pseudo + ")");
        }
        else
        {
            jsonLoaded = File.ReadAllText(Application.persistentDataPath + "/Maps/Local/" + mapName + ".txt");
            print("Loaded map \"" + mapName + "\" from local persitent data");
        }

        mapInfo = JsonUtility.FromJson<MapInfo>(jsonLoaded);

        LoadPlayer();
        LoadShakeCamera();
        LoadMapObjects();
        LoadMapSlots();
        LoadMapFurnitures(editor);
        LoadMapCollectables();
        //LoadMapNPCs(editor);
        LoadMapPerfs(editor);
        LoadTutorialInfo();
    }

    private void LoadPlayer()
    {
        GameObject player = GameObject.Find("Player");
        player.transform.position = mapInfo.playerInfo.position;
    }

    private void LoadShakeCamera()
    {
        GameObject shakeCamera = GameObject.Find("CameraWhenShakeParent");
        shakeCamera.transform.position = mapInfo.shakeCameraInfo.position;
        shakeCamera.transform.rotation = Quaternion.Euler(
            mapInfo.shakeCameraInfo.rotation.x,
            mapInfo.shakeCameraInfo.rotation.y,
            mapInfo.shakeCameraInfo.rotation.z);
    }

    /// <summary>
    /// Instantiates the GameObject, assign its transform,
    /// its id based on ObjectCtrl and its textures based on textureCtrl
    /// </summary>
    /// <returns>The created GameObject</returns>
    private GameObject LoadObjectBasics(int id, string prefabName, Vector3 position, Vector3 localScale,
        Vector3 rotation, List<ObjectTexture> objectsTexture)
    {
        GameObject prefab = gameController.GetPrefab(prefabName);
        Transform prefabTransform = prefab.transform;
        prefabTransform.position = position;

        // INSTANTIATE
        GameObject o = Instantiate(prefab, prefabTransform.position, prefabTransform.rotation);
        o.name = prefabName;
        o.transform.localScale = localScale;
        o.transform.rotation = Quaternion.Euler(rotation.x, rotation.y, rotation.z);

        // Edit id
        ObjectController objController = o.GetComponent<ObjectController>();
        objController.Init(gameController);
        if (id == 0)
            objController.IncreaseID();
        else
            objController.id = id;

        gameController.ListObjectCtrl.Add(objController);

        // Assign textures
        if (objectsTexture != null && objectsTexture.Count > 0)
        {
            foreach (var ot in objectsTexture)
            {
                TextureController tc =
                    ot.objectName == "parent" ?
                    o.GetComponent<TextureController>()
                    : o.transform.FindDeepChild(ot.objectName).GetComponent<TextureController>();

                if (tc != null)
                    tc.currentMaterialIndex = ot.currentTextureIndex;
            }

            objController.AssignCorrespondingTextures();
        }
        return o;
    }

    private void LoadMapObjects()
    {
        foreach (MapObject mapObject in mapInfo.mapObjects)
        {
            LoadObjectBasics(
                mapObject.id,
                mapObject.prefabName,
                mapObject.position,
                mapObject.localScale,
                mapObject.rotation,
                mapObject.objectsTexture);

            gameController.currentObjectIndex++;
        }
    }
    
    // Setting id and display type
    private void UpdateSlotController(SlotController slotController, int idExpected, DisplayType displayType)
    {
        slotController.idExpected = idExpected;
        slotController.idOnIt = idExpected;
        slotController.displayType = displayType;
        slotController.alreadySet = true;
        slotController.EnableColliders(false);

        gameController.ListSlotCtrl.Add(slotController);
        gameController.currentObjectIndex++;
    }

    // Loads non-inner slots
    private void LoadMapSlots()
    {
        foreach (var mapSlot in mapInfo.mapSlots)
        {
            var o = LoadObjectBasics(mapSlot.id, mapSlot.prefabName, mapSlot.position, mapSlot.localScale, mapSlot.rotation, null);
            SlotController slotController = o.GetComponent<SlotController>();
            UpdateSlotController(slotController, mapSlot.idExpected, mapSlot.displayType);

            gameController.currentObjectIndex++;
        }
    }

    private void LoadMapFurnitures(bool editor)
    {
        foreach (var mf in mapInfo.mapFurnitures)
        {
            var o = LoadObjectBasics(mf.id, mf.prefabName, mf.position, mf.localScale, mf.rotation, mf.objectsTexture);

            // Set inner slots transform and set to no parent if in game
            var alreadyExistingSlots = o.GetComponentsInChildren<SlotController>();
            for (int i = 0; i < alreadyExistingSlots.Length; i++)
            {
                var slotController = alreadyExistingSlots[i];
                var mf_innerSlot = mf.innerSlots[i];

                // Transform
                slotController.transform.position = mf_innerSlot.position;
                slotController.transform.localScale = mf_innerSlot.localScale;
                slotController.transform.rotation = Quaternion.Euler(
                    mf_innerSlot.rotation.x,
                    mf_innerSlot.rotation.y,
                    mf_innerSlot.rotation.z);

                // Set no parent
                if (!editor)
                    slotController.gameObject.transform.parent = null;

                // Setting inner slot id !
                var objController = slotController.GetComponent<ObjectController>();
                objController.Init(gameController);
                if (mf_innerSlot.id == 0)
                    objController.IncreaseID();
                else
                    objController.id = mf_innerSlot.id;
                gameController.currentObjectIndex++;

                UpdateSlotController(slotController, mf_innerSlot.idExpected, mf_innerSlot.displayType);
            }
            
            if (!editor)
                FurnitureEditorToInGame(o);
            
            var interactiveLockerController = o.GetComponent<InteractiveLockerController>();
            if (interactiveLockerController)
                gameController.ListLockers.Add(interactiveLockerController);

            gameController.currentObjectIndex++;
        }
    }

    private void RemoveRbHaloAndColCtrl(GameObject o)
    {
        var colCtrl = o.GetComponent<CollectableController>();
        if (colCtrl != null)
            Destroy(colCtrl);

        var rb = o.GetComponent<Rigidbody>();
        if (rb != null)
            Destroy(rb);

        var halo = o.GetComponent("Halo");
        if (halo != null)
            Destroy(halo);
    }

    /* Changes editor furniture to in-game furniture (non-interactive only)
     * Untag all
     * Remove CollectableCtrl, Halo, Rigidbody
     */
    private void FurnitureEditorToInGame(GameObject o)
    {
        o.tag = "Untagged";
        FurnitureEditorToInGameRec(o.transform);

        RemoveRbHaloAndColCtrl(o);
    }
    
    // Untags recursively all children
    private void FurnitureEditorToInGameRec(Transform o)
    {
        foreach (Transform child in o.transform)
        {
            if (child.tag == "collider")
                child.tag = "Untagged";
            FurnitureEditorToInGameRec(child);
        }
    }

    private void LoadMapCollectables()
    {
        List<CollectableController> allCollectables = new List<CollectableController>();
        foreach (var col in mapInfo.mapCollectables)
        {
            var o = LoadObjectBasics(col.id, col.prefabName, col.position, col.localScale, col.rotation, col.objectsTexture);

            CollectableController colController = o.GetComponent<CollectableController>();
            colController.displayType = col.displayType;
            colController.checkRotation = col.checkRotation;
            colController.auditive = col.auditive;
            colController.slotId = col.slotIdExpected;
            colController.slotIdExpected = col.slotIdExpected;

            if (colController.collectableType == CollectableType.VERTICAL || colController.collectableType == CollectableType.DISC)
                o.GetComponent<Rigidbody>().isKinematic = true;

            WaterBottleController waterBottleController;
            if ((waterBottleController = o.GetComponent<WaterBottleController>()) != null)
                waterBottleController.SetInitialValue();

            colController.UpdateChildMaterials();
            colController.ApplyDisplay(true);
            allCollectables.Add(colController);

            gameController.currentObjectIndex++;

            if (colController.auditive)
                gameController.nbCollectablesAuditive++;
            else if (colController.collectableType == CollectableType.REGULAR || colController.collectableType == CollectableType.DISC)
                gameController.nbCollectablesRegular++;
            else if (colController.collectableType == CollectableType.VERTICAL)
                gameController.nbCollectablesVertical++;
        }
        gameController.ListCollectableCtrl = allCollectables;
    }

    private void LoadMapNPCs(bool editor)
    {
        foreach (var mapNPC in mapInfo.npcs)
        {
            var o = LoadObjectBasics(mapNPC.id, mapNPC.prefabName, mapNPC.position, mapNPC.localScale, mapNPC.rotation, null);

            NPCController npcController = o.GetComponent<NPCController>();
            npcController.buttonFinish = mapNPC.buttonFinish;
            npcController.buttonTalk = mapNPC.buttonTalk;

            var npcButtonTalkController = o.GetComponentInChildren<NPCButtonTalkController>();
            npcButtonTalkController.fullText = mapNPC.talkText;
            
            if (!editor)
            {
                RemoveRbHaloAndColCtrl(o);
                o.tag = "npc";
                RemoveColliderTag(o.transform.Find("Model").gameObject);
            }
            else
                npcController.DisableTalkToMe();

            gameController.ListNPC.Add(o);
            gameController.currentObjectIndex++;
        }
    }

    private void RemoveColliderTag(GameObject o)
    {
        o.tag = "Untagged";
        foreach (Transform child in o.transform)
        {
            child.tag = "Untagged";
            RemoveColliderTag(child.gameObject);
        }
    }

    private void LoadMapPerfs(bool editor)
    {
        foreach (MapPerf mapPerf in mapInfo.perfs)
        {
            var o = LoadObjectBasics(mapPerf.id, mapPerf.prefabName, mapPerf.position, mapPerf.localScale, mapPerf.rotation, null);

            var waterController = o.GetComponentInChildren<WaterController>();
            waterController.WaterLevel = mapPerf.waterLevel;
            waterController.waterLevelExpected = mapPerf.waterLevel;
            waterController.alreadySet = true;
            
            if (!editor)
                FurnitureEditorToInGame(o);

            gameController.ListPerfs.Add(o);
            gameController.currentObjectIndex++;
        }
    }
    
    private void LoadTutorialInfo()
    {
        gameController.isTutorial = mapInfo.isTutorial;
        if (mapInfo.isTutorial)
            gameController.hudController.CreateModal("Tutoriel", mapInfo.tutorialData, true);
    }

    public void AssignObjectIds()
    {
        foreach (ObjectController o in gameController.ListObjectCtrl)
            o.IncreaseID();
    }

    /// <summary>
    /// Fetch versions on the server and creates/updates maps 
    /// (and versions in the version file) if not up to date
    /// </summary>
    public void RefreshMaps()
    {
        Debug.Log("Enter RefreshMaps");
        refreshingMaps = true;
        
        StartCoroutine(
            mapsController.GetAllMaps((resBool, res) =>
            {
                Debug.Log("Le result de la coco");
                if (resBool)
                {
                    foreach (var currMap in res)
                    {
                        var version = -1;
                        int.TryParse(currMap.version.ToString(), out version);

                        MapsVersions mv = new MapsVersions(true);
                        mv.UpdateAndSaveMapLocal(currMap.name, version, currMap.json);
                    }
                    //Thread.Sleep(5000);
                }
                else
                {
                    Debug.Log("Erreur");
                    Debug.Log(res);
                }
                refreshingMaps = false;
            })
        );
    }

    public bool GetRefreshingMaps()
    {
        return refreshingMaps;
    }

    public bool IsCurrentMapNative()
    {
        return LevelProperties.levelTypeToLoad == LevelProperties.LevelType.RESOURCE;
    }
}
