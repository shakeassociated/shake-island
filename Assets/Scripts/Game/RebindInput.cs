﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using System.Collections;

namespace TeamUtility.IO
{
	public class RebindInput : MonoBehaviour, IPointerDownHandler 
	{
		public enum RebindType
		{
			Keyboard, GamepadButton, GamepadAxis
		}

		[SerializeField]
		[FormerlySerializedAs("m_keyDescription")]
		private Text _keyDescription;

		[SerializeField]
		[FormerlySerializedAs("m_inputConfigName")]
		private string _inputConfigName;

		[SerializeField]
		[FormerlySerializedAs("m_axisConfigName")]
		private string _axisConfigName;

		[SerializeField]
		[FormerlySerializedAs("m_timeout")]
		private float _timeout;

		[SerializeField]
		[FormerlySerializedAs("m_changePositiveKey")]
		private bool _changePositiveKey;

		[SerializeField]
		[FormerlySerializedAs("m_changeAltKey")]
		private bool _changeAltKey;

		[SerializeField]
		[FormerlySerializedAs("m_allowAnalogButton")]
		private bool _allowAnalogButton;

		[SerializeField]
		[FormerlySerializedAs("m_rebindType")]
		private RebindType _rebindType;
		
		private AxisConfiguration _axisConfig;

		private static string[] _axisNames = new string[] { "X", "Y", "3rd", "4th", "5th", "6th", "7th", "8th", "9th", "10th" };
		
		private void Awake()
		{
			InitializeAxisConfig();
			
			//	The axis config needs to be reinitialized because loading can invalidate
			//	the input configurations
			InputManager.Instance.Loaded += InitializeAxisConfig;
			InputManager.Instance.ConfigurationDirty += HandleConfigurationDirty;
		}
		
		private void OnDestroy()
		{
			if(InputManager.Instance != null)
			{
				InputManager.Instance.Loaded -= InitializeAxisConfig;
				InputManager.Instance.ConfigurationDirty -= HandleConfigurationDirty;
			}
		}
		
		private void InitializeAxisConfig()
		{
			_axisConfig = InputManager.GetAxisConfiguration(_inputConfigName, _axisConfigName);
			if(_axisConfig != null)
			{
				if(_rebindType == RebindType.Keyboard || _rebindType == RebindType.GamepadButton)
				{
					if(_changePositiveKey)
					{
						if(_changeAltKey)
							_keyDescription.text = _axisConfig.altPositive == KeyCode.None ? "" : _axisConfig.altPositive.ToString();
						else
							_keyDescription.text = _axisConfig.positive == KeyCode.None ? "" : _axisConfig.positive.ToString();
					}
					else
					{
						if(_changeAltKey)
							_keyDescription.text = _axisConfig.altNegative == KeyCode.None ? "" : _axisConfig.altNegative.ToString();
						else
							_keyDescription.text = _axisConfig.negative == KeyCode.None ? "" : _axisConfig.negative.ToString();
					}
				}
				else
				{
					_keyDescription.text = _axisNames[_axisConfig.axis];
				}
			}
			else
			{
				_keyDescription.text = "";
				Debug.LogError(string.Format(@"Input configuration '{0}' does not exist or axis '{1}' does not exist", _inputConfigName, _axisConfigName));
			}
		}

		private void HandleConfigurationDirty(string configName)
		{
			if(configName == _inputConfigName)
				InitializeAxisConfig();
		}

		public void OnPointerDown(PointerEventData data)
		{
			StartCoroutine(StartInputScanDelayed());
		}

		private IEnumerator StartInputScanDelayed()
		{
			yield return null;

			if(!InputManager.IsScanning && _axisConfig != null)
			{
				_keyDescription.text = "...";
				
				ScanSettings settings;
				settings.joystick = null;
				settings.cancelScanButton = null;
				settings.timeout = _timeout;
				settings.userData = null;
				settings.scanFlags = ScanFlags.Key;
				InputManager.StartScan(settings, HandleKeyScan);
			}
		}
		
		private bool HandleKeyScan(ScanResult result)
		{
			//	When you return false you tell the InputManager that it should keep scaning for other keys
			if(!IsKeyValid(result.key))
				return false;
			
			//	The key is KeyCode.None when the timeout has been reached or the scan has been canceled
			if(result.key != KeyCode.None)
			{
				//	If the key is KeyCode.Backspace clear the current binding
				result.key = (result.key == KeyCode.Backspace) ? KeyCode.None : result.key;
				if(_changePositiveKey)
				{
					if(_changeAltKey)
						_axisConfig.altPositive = result.key;
					else
						_axisConfig.positive = result.key;
				}
				else
				{
					if(_changeAltKey)
						_axisConfig.altNegative = result.key;
					else
						_axisConfig.negative = result.key;
				}
				_keyDescription.text = (result.key == KeyCode.None) ? "" : result.key.ToString();
			}
			else
			{
				KeyCode currentKey = GetCurrentKeyCode();
				_keyDescription.text = (currentKey == KeyCode.None) ? "" : currentKey.ToString();
			}
			return true;
		}

		private bool IsKeyValid(KeyCode key)
		{
			bool isValid = true;

			if(_rebindType == RebindType.Keyboard)
			{
				if((int)key >= (int)KeyCode.JoystickButton0)
					isValid = false;
				else if(key == KeyCode.LeftApple || key == KeyCode.RightApple)
					isValid = false;
				else if(key == KeyCode.LeftWindows || key == KeyCode.RightWindows)
					isValid = false;
			}
			else
			{
				isValid = false;
			}

			return isValid;
		}

		private KeyCode GetCurrentKeyCode()
		{
			if(_rebindType == RebindType.GamepadAxis)
				return KeyCode.None;

			if(_changePositiveKey)
			{
				if(_changeAltKey)
					return _axisConfig.altPositive;
				else
					return _axisConfig.positive;
			}
			else
			{
				if(_changeAltKey)
					return _axisConfig.altNegative;
				else
					return _axisConfig.negative;
			}
		}
	}
}