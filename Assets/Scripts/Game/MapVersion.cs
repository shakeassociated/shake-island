﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[System.Serializable]
public class MapVersion
{
    public string name;
    public int version;

    public MapVersion(string name, int version)
    {
        this.name = name;
        this.version = version;
    }
}

[System.Serializable]
public class MapsVersions
{
    public List<MapVersion> listMapsVersions;

    public MapsVersions(bool fetch)
    {
        listMapsVersions = new List<MapVersion>();
        if (fetch)
            FetchAllMapsVersions();
    }
    
    public void FetchAllMapsVersions()
    {
        string path = @"Levels\map_versions.txt";
        if (File.Exists(path))
        {
            MapsVersions mapsVersions = JsonUtility.FromJson<MapsVersions>(File.ReadAllText(path));
            listMapsVersions = mapsVersions.listMapsVersions;
        }
    }
    
    public int GetVersion(string mapName)
    {
        foreach (var mv in listMapsVersions)
        {
            if (mv.name == mapName)
                return mv.version;
        }
        return -1;
    }

    public void SaveThis()
    {
        string jsonGenerated = JsonUtility.ToJson(this);
        string path = @"Levels\map_versions.txt";

        File.WriteAllText(path, jsonGenerated);
    }

    public void UpdateAndSaveVersion(string mapName, int version)
    {
        bool found = false;
        foreach (var mv in listMapsVersions)
        {
            if (mv.name == mapName)
            {
                mv.version = version;
                found = true;
            }
        }
        if (!found)
            listMapsVersions.Add(new MapVersion(mapName, version));

        SaveThis();
    }

    public void UpdateAndSaveMapLocal(string mapName, int newVersion, string json)
    {
        /* Update version */
        bool found = false, recreateJson = true;
        foreach (var mv in listMapsVersions)
        {
            if (mv.name == mapName)
            {
                found = true;
                if (mv.version >= newVersion)
                    recreateJson = false;
                else
                    mv.version = newVersion;
            }
        }
        if (!found)
            listMapsVersions.Add(new MapVersion(mapName, newVersion));
        SaveThis();

        /* Update JSON file */
        if (recreateJson)
        {
            File.WriteAllText(@"Levels\" + mapName + ".txt", json);
        }
    }
}
