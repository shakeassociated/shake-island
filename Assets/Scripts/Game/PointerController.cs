﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PointerController : MonoBehaviour
{
    private new Camera camera;
    private PlayerController playerController;
    SlotController lastSlotController;
    GameController gameController;

    public bool hasHitDropPoint;
    private RaycastHit hitDropPoint;

    InteractionButtonController interactionButtonCtrl;

    HUDController hudCtrl;
    CustomTimer timerInteraction;

    bool isHoldingCollectableRegular, isHoldingCollectableVertical, isHoldingCollectableAuditive;

    void Start()
    {
        camera = GetComponent<Camera>();
        playerController = transform.parent.GetComponent<PlayerController>();
        gameController = playerController.gameController;
        hudCtrl = FindObjectOfType<HUDController>();
        lastSlotController = null;

        hasHitDropPoint = false;
        interactionButtonCtrl = null;
        hudCtrl.ChangePointerState(HUDController.PointerState.NONE);
        timerInteraction = new CustomTimer(0.3f);

        isHoldingCollectableRegular = false;
        isHoldingCollectableVertical = false;
        isHoldingCollectableAuditive = false;
    }

    void Update()
    {
        if (!gameController)
            gameController = playerController.gameController;

        RaycastHit hit;
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);
        bool rayCast = Physics.Raycast(ray, out hit, 20);

        if (timerInteraction.IsOver() && gameController.gameState == GameController.GameState.RESTORE)
        {
            if (playerController.HasACollected())
                Drop(rayCast, hit);
            else
                Collect(rayCast, hit);
        }

        ButtonInteraction(rayCast, hit);
        UpdateScoreTimers();
        timerInteraction.Update();
    }

    void UpdateScoreTimers()
    {
        if (isHoldingCollectableRegular)
            gameController.scoreController.timerCollectablesRegular += Time.deltaTime;
        else if (isHoldingCollectableVertical)
            gameController.scoreController.timerCollectablesVertical += Time.deltaTime;
        else if (isHoldingCollectableAuditive)
            gameController.scoreController.timerCollectablesAuditive += Time.deltaTime;
    }

    GameObject GetCollectableFromInnerCollider(GameObject firstCollided)
    {
        GameObject res = firstCollided;
        while (!res.GetComponent<CollectableController>())
        {
            res = res.transform.parent.gameObject;
        }
        return res;
    }

    void Collect(bool rayCast, RaycastHit hit)
    {
        if (hasHitDropPoint)
            DisableSlotTurning();

        if (rayCast && string.Compare(hit.collider.gameObject.tag, "collider") == 0)
        {
            var firstCollided = hit.collider.gameObject;
            GameObject collided = GetCollectableFromInnerCollider(firstCollided);
            CollectableController colController = collided.GetComponent<CollectableController>();

            hudCtrl.ChangePointerState(HUDController.PointerState.COLLECT);

            // Left Click
            if (Input.GetMouseButton(0))
            {
                if (colController.slotId != 0)
                    hudCtrl.UpdateNbCollectables(false);

                colController.Take();
                playerController.Collect(collided);

                if (colController.IsEditorType())
                    gameController.ChangeLayersToIgnore(true);
                hudCtrl.DisplayCollectedObject(collided);
                hudCtrl.ChangePointerState(HUDController.PointerState.NONE);

                if (colController.auditive)
                    isHoldingCollectableAuditive = true;
                else if (colController.collectableType == CollectableType.REGULAR)
                    isHoldingCollectableRegular = true;
                else
                    isHoldingCollectableVertical = true;

                gameController.EnableSlotsColliders(!colController.IsEditorType());
                timerInteraction.Restart();
            }

        }
        else
        {
            hudCtrl.ChangePointerState(HUDController.PointerState.NONE);
        }
    }

    void Drop(bool rayCast, RaycastHit hit)
    {
        bool canDrop = false;
        bool interversion = false;
        bool intersectSlot = false;
        SlotController slotController = null;
        CollectableController colController = null;
        CollectableController objectInHandController = playerController.collected.GetComponent<CollectableController>();
        var colType = objectInHandController.collectableType;
        Vector3 newPosition = Vector3.zero;

        if (rayCast)
        {
            if (objectInHandController.IsEditorTypeAndCorrectTag(hit.collider.tag))
            {
                objectInHandController.DisplayRenderer(true);
                objectInHandController.transform.position = hit.point;
                newPosition = hit.point;

                // Setting to correct first rotation
                if (objectInHandController.IsEditorVerticalType())
                {
                    var t = objectInHandController.transform;
                    var hitT = hit.collider.transform;
                    if (t.rotation.eulerAngles.y != hitT.rotation.eulerAngles.y)
                        t.eulerAngles = new Vector3(t.eulerAngles.x, hitT.eulerAngles.y, t.eulerAngles.z);
                }

                canDrop = true;
                hasHitDropPoint = true;
                hudCtrl.ChangePointerState(HUDController.PointerState.DROP);
            }
            else if (hit.collider.gameObject.CompareTag("slot") || hit.collider.gameObject.CompareTag("collider"))
            {
                intersectSlot = string.Compare(hit.collider.gameObject.tag, "slot") == 0;
                bool sameType = false;
                if (intersectSlot)
                {
                    slotController = hit.collider.gameObject.GetComponent<SlotController>();
                    var slotType = slotController.collectableExpected;

                    sameType = objectInHandController.collectableType == slotType;
                }
                else
                {
                    colController = GetCollectableFromInnerCollider(hit.collider.gameObject).GetComponent<CollectableController>();
                    if (colController.slotId != 0)
                        sameType = objectInHandController.collectableType == colController.collectableType;
                }

                if (sameType)
                {
                    // Interversion if is a collectable or the slot is not empty
                    interversion = !intersectSlot || slotController.idOnIt != 0;

                    if (!intersectSlot)
                    {
                        GameObject slotOfCollectable = gameController.GetObjectById(colController.slotId);
                        slotController = slotOfCollectable.GetComponent<SlotController>();
                    }

                    // Error if object intersected not binded to a slot
                    if (slotController)
                    {
                        // UI
                        hudCtrl.ChangePointerState(HUDController.PointerState.DROP);
                        canDrop = slotController.correctlyTurned;

                        // Disable previous slot if different (when you move from one slot to another)
                        if (hasHitDropPoint
                            && gameController.GetIdOf(hitDropPoint.collider.gameObject) != gameController.GetIdOf(hit.collider.gameObject))
                        {
                            DisableSlotTurning();
                        }

                        // Reassign hitDropPoint
                        hasHitDropPoint = true;
                        hitDropPoint = hit;

                        slotController.TurnObjectOnTop(playerController.collected);
                        lastSlotController = slotController;

                        // Particular display (radio for example)
                        var sameDisplayType = objectInHandController.displayType == slotController.displayType;
                        objectInHandController.ApplyDisplay(sameDisplayType);
                    }
                }
                else
                {
                    if (hasHitDropPoint)
                    {
                        DisableSlotTurning();
                        lastSlotController = null;
                        hasHitDropPoint = false;
                    }
                    objectInHandController.DisplayRenderer(false);

                    hudCtrl.ChangePointerState(HUDController.PointerState.NONE);
                }
            }
            else
            {
                if (hasHitDropPoint)
                {
                    DisableSlotTurning();
                    lastSlotController = null;
                    hasHitDropPoint = false;
                }
                objectInHandController.DisplayRenderer(false);

                hudCtrl.ChangePointerState(HUDController.PointerState.NONE);
            }
        }
        else
        {
            if (hasHitDropPoint)
            {
                DisableSlotTurning();
                lastSlotController = null;
                hasHitDropPoint = false;
            }
            objectInHandController.DisplayRenderer(false);

            hudCtrl.ChangePointerState(HUDController.PointerState.NONE);
        }

        bool buttonIPressed;
        if (TeamUtility.IO.InputManager.Instance)
            buttonIPressed = TeamUtility.IO.InputManager.GetAxisConfiguration("KeyboardAndMouse", "Inventory").GetButtonDown();
        else
            buttonIPressed = Input.GetKeyDown(KeyCode.I);

        // Right Click Or Inventory (Delete)
        if (gameController.gameType == GameController.GameType.EDITOR && (Input.GetMouseButton(1) || buttonIPressed))
        {
            //if (colType == CollectableType.FURNITURE_EDITOR)
                gameController.ChangeLayersToIgnore(false);

            hudCtrl.StopDisplayCollectedObject();
            if (interversion || intersectSlot)
            {
                if (hasHitDropPoint)
                {
                    DisableSlotTurning();
                    lastSlotController = null;
                    hasHitDropPoint = false;
                }
            }
            gameController.ListCollectableCtrl.Remove(objectInHandController);
            playerController.DeleteCollected();
            gameController.EnableSlotsColliders(false);
        }
        // Left Click (Actual Drop)
        else if (Input.GetMouseButton(0) && canDrop
           && (objectInHandController.IsEditorType() ? objectInHandController.canBePlaced : true))
        {
            if (objectInHandController.IsEditorType())
            {
                gameController.ChangeLayersToIgnore(false);
                playerController.Drop(newPosition);
                hudCtrl.StopDisplayCollectedObject();
                gameController.EnableSlotsColliders(false);
            }
            else
            {
                if (interversion)
                {
                    if (intersectSlot)
                        colController = slotController.GetObjectOnIt().GetComponent<CollectableController>();

                    playerController.Drop(slotController);
                    colController.Take();
                    playerController.Collect(colController.gameObject);
                    hudCtrl.DisplayCollectedObject(colController.gameObject);
                }
                else
                {
                    playerController.Drop(slotController);
                    hudCtrl.StopDisplayCollectedObject();
                }
                DisableSlotTurning();
                objectInHandController.ApplyDisplay(slotController.displayType == objectInHandController.displayType);
            }

            isHoldingCollectableVertical = false;
            isHoldingCollectableRegular = false;
            isHoldingCollectableAuditive = false;
            hasHitDropPoint = false;
            hudCtrl.UpdateNbCollectables(true);

            gameController.EnableSlotsColliders(interversion); // enable only if interversion
            timerInteraction.Restart();
        }
    }

    void DisableSlotTurning()
    {
        SlotController slotController = lastSlotController;
        if (slotController)
            slotController.StopTurningObject();
    }

    void ButtonInteraction(bool rayCast, RaycastHit hit)
    {
        if (rayCast)
        {
            if (string.Compare(hit.collider.gameObject.tag, "interactionButton") == 0)
            {
                interactionButtonCtrl = hit.collider.gameObject.GetComponent<InteractionButtonController>();
                interactionButtonCtrl.MouseOnIt(true);

                // Left Click
                if (Input.GetMouseButton(0))
                    interactionButtonCtrl.DoAction();
            }
            else if (interactionButtonCtrl != null)
            {
                interactionButtonCtrl.MouseOnIt(false);
                interactionButtonCtrl = null;
            }
        }
    }
}
