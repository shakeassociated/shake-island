﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnackMachineButtonController : InteractionButtonController
{
    public enum SnackType { BANANA, BLACK_CHOCOLATE, MILK_CHOCOLATE, RED_FRUITS, THROW };
    public SnackType snackType;
    public GameObject snackPrefab;

    PlayerController playerController;
    HUDController hudController;

    private void Awake()
    {
        playerController = FindObjectOfType<PlayerController>();
    }

    public override void Start()
    {
        base.Start();
        hudController = transform.parent.parent.GetComponent<ObjectController>().GameController.hudController;
    }

    public override void DoAction()
    {
        if (timerReclick.IsOver())
        {
            if (snackType != SnackType.THROW && playerController.collected == null)
                CollectFromPrefab();
            else if (snackType == SnackType.THROW && playerController.collected != null && playerController.collected.name == "Snack")
                ThrowCurrSnack();
        }
        base.DoAction();
    }

    private int SnackTypeToTextureId()
    {
        switch (snackType)
        {
            case SnackType.BANANA:
                return 0;
            case SnackType.BLACK_CHOCOLATE:
                return 1;
            case SnackType.RED_FRUITS:
                return 2;
            case SnackType.MILK_CHOCOLATE:
                return 3;
            default:
                return 0;
        }
    }

    private void CollectFromPrefab()
    {
        GameObject o = Instantiate(snackPrefab);
        o.name = snackPrefab.name;

        // Setting Texture Controller
        o.transform.FindDeepChild("Snack").GetComponent<TextureController>().currentMaterialIndex = SnackTypeToTextureId();

        ObjectController objController = o.GetComponent<ObjectController>();
        objController.IncreaseID(); // Setting id !
        GameController.ListObjectCtrl.Add(objController);

        var colController = o.GetComponent<CollectableController>();
        colController.Take();
        playerController.Collect(o);
        hudController.DisplayCollectedObject(o);
        GameController.EnableSlotsColliders(!colController.IsEditorType());
    }

    private void ThrowCurrSnack()
    {
        foreach (var o in GameController.ListObjectCtrl)
        {
            if (o.id == playerController.collected.GetComponent<ObjectController>().id)
                GameController.ListObjectCtrl.Remove(o);
        }
        playerController.Throw();
        hudController.StopDisplayCollectedObject();
    }
}
