﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaterController : MonoBehaviour
{
    float speedFill, sizeToFill;
    int waterLevel, maxLevel, waterLevelMl;
    bool isFilling;
    public int waterLevelExpected;

    public int WaterLevel { get { return waterLevel; } set { if (waterLevel > -1 && waterLevel < 9) { waterLevel = value; } } }
    public bool alreadySet;

    GameObject content;
    TextMesh contentText;
    Vector3 initialLocalScale;

    private void Start()
    {
        if (!alreadySet)
            waterLevel = 0;

        speedFill = 0.04f;
        sizeToFill = 0.6f;
        maxLevel = 8;
        waterLevelMl = 100;
        isFilling = false;

        content = transform.Find("Content").gameObject;
        contentText = transform.parent.Find("Labels").Find("ContentText").GetComponent<TextMesh>();
        contentText.gameObject.SetActive(false);

        initialLocalScale = transform.localScale;

        UpdateWaterLevel();
    }

    public void ChangeWaterLevel(bool increase)
    {
        if (!isFilling && (increase ? waterLevel < maxLevel : waterLevel > 0))
            StartCoroutine(ChangeWaterAnimation(increase));
    }

    private IEnumerator ChangeWaterAnimation(bool increase)
    {
        isFilling = true;

        float yAdded = 0f;
        int unit = (increase ? 1 : -1);

        while (increase ? yAdded < sizeToFill : yAdded < sizeToFill)
        {
            var newScale = transform.localScale;
            yAdded += speedFill;
            newScale.y += speedFill * unit;
            transform.localScale = newScale;

            var newContentTextPos = contentText.transform.position;
            newContentTextPos.y = content.transform.position.y;
            contentText.transform.position = newContentTextPos;

            waterLevelMl += 6 * unit;
            contentText.text = waterLevelMl + " mL";

            yield return new WaitForSeconds(0.03f);
        }

        waterLevel = waterLevel + unit;
        UpdateWaterLevel();

        isFilling = false;
    }

    public void UpdateWaterLevel()
    {
        var newScale = initialLocalScale;
        newScale.y += sizeToFill * waterLevel;
        transform.localScale = newScale;

        waterLevelMl = ((waterLevel + 1) * 100);
        contentText.text = waterLevelMl + " mL";

        var newContentTextPos = contentText.transform.position;
        newContentTextPos.y = content.transform.position.y;
        contentText.transform.position = newContentTextPos;
    }

    public bool CorrectWaterLevel()
    {
        return waterLevel == waterLevelExpected;
    }

    public void Shake()
    {
        waterLevel = 0;
        UpdateWaterLevel();
    }
}
