﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.SceneManagement;
using UnityEngine.AI;

public class GameController : MonoBehaviour
{
    public bool loadJSON, debugMode, shake, modeSoutenance;
    private bool loadedJSON;
    public List<GameObject> prefabs = new List<GameObject>();
    public GameObject player;
    public GameObject CameraWhenShakeParent;

    public int currentObjectIndex;
    public int nbCollectablesRegular, nbCollectablesVertical, nbCollectablesAuditive;

    public float timerGameSeconds;
    public float timeSpentResolving;

    private float timeLeft;

    bool setIds;
    public enum GameType { GAME, EDITOR };
    public GameType gameType;
    public bool isTutorial;
    public string tutorialData;
    public int nbTutorials;

    public enum GameState { INSPECT, SHAKE, RESTORE, PAUSE, OBJECTSELECTOR };
    public GameState gameState;
    GameState gameStateBeforePause;

    public MapController mapController;
    public ScoreController scoreController;
    public FirstPersonController fpsController;
    PlayerController playerController;
    CameraShake cameraShake;
    PointerController pointerController;
    public HUDController hudController;
    MusicController musicController;

    #region List of objects in game
    private List<ObjectController> listObjectCtrl;
    public List<ObjectController> ListObjectCtrl
    {
        get
        {
            if (listObjectCtrl.Count > 0)
                return listObjectCtrl;
            return new List<ObjectController>(FindObjectsOfType<ObjectController>());
        }
        set { listObjectCtrl = value; }
    }

    private List<SlotController> listSlotCtrl;
    public List<SlotController> ListSlotCtrl
    {
        get
        {
            if (listSlotCtrl.Count > 0)
                return listSlotCtrl;
            return new List<SlotController>(FindObjectsOfType<SlotController>());
        }
        set { listSlotCtrl = value; }
    }

    private List<CollectableController> listCollectableCtrl;
    public List<CollectableController> ListCollectableCtrl
    {
        get
        {
            if (listCollectableCtrl.Count > 0)
                return listCollectableCtrl;
            return new List<CollectableController>(FindObjectsOfType<CollectableController>());
        }
        set { listCollectableCtrl = value; }
    }

    private List<GameObject> listNPC;
    public List<GameObject> ListNPC
    {
        get
        {
            if (listNPC.Count > 0)
                return listNPC;
            return new List<GameObject>(GameObject.FindGameObjectsWithTag("npc"));
        }
        set { listNPC = value; }
    }

    private List<GameObject> listPerfs;
    public List<GameObject> ListPerfs
    {
        get
        {
            if (listPerfs.Count > 0)
                return listPerfs;
            return new List<GameObject>(GameObject.FindGameObjectsWithTag("perf"));
        }
        set { listPerfs = value; }
    }

    private List<InteractiveLockerController> listLockers;
    public List<InteractiveLockerController> ListLockers
    {
        get
        {
            if (listLockers.Count > 0)
                return listLockers;
            return new List<InteractiveLockerController>(FindObjectsOfType<InteractiveLockerController>());
        }
        set { listLockers = value; }
    }
    #endregion

    private void Awake()
    {
        player = GameObject.Find("Player");

        fpsController = player.GetComponent<FirstPersonController>();
        playerController = player.GetComponent<PlayerController>();
        pointerController = player.GetComponentInChildren<PointerController>();

        CameraWhenShakeParent = GameObject.Find("CameraWhenShakeParent");
        cameraShake = CameraWhenShakeParent.GetComponent<CameraShake>();
        scoreController = GetComponent<ScoreController>();
        hudController = GetComponent<HUDController>();

        mapController = GetComponent<MapController>();

        musicController = GetComponent<MusicController>();
    }

    void Start()
    {
        // Lists of objects in game
        listObjectCtrl = new List<ObjectController>();
        listSlotCtrl = new List<SlotController>();
        listCollectableCtrl = new List<CollectableController>();
        listNPC = new List<GameObject>();
        listPerfs = new List<GameObject>();
        listLockers = new List<InteractiveLockerController>();

        mapController.Init();

        currentObjectIndex = 1;

        setIds = false;

        timeLeft = timerGameSeconds;
        timeSpentResolving = 0f;

        if (gameType == GameType.GAME)
            gameState = GameState.INSPECT;
        else
        {
            gameState = GameState.RESTORE;
            hudController.SetAsLevelEditor();
        }
        gameStateBeforePause = gameState;

        if (gameType != GameType.EDITOR)
            musicController.ChangeState(gameState);

        loadedJSON = false;
    }

    public void Restart()
    {
        // Lists of objects in game
        listObjectCtrl.Clear();
        listSlotCtrl.Clear();
        listCollectableCtrl.Clear();
        listNPC.Clear();
        listPerfs.Clear();
        listLockers.Clear();

        mapController.Init();

        currentObjectIndex = 1;

        setIds = false;

        timeLeft = timerGameSeconds;
        timeSpentResolving = 0f;

        hudController.Restart();

        if (gameType == GameType.GAME)
            gameState = GameState.INSPECT;
        else
        {
            gameState = GameState.RESTORE;
            hudController.SetAsLevelEditor();
        }
        gameStateBeforePause = gameState;
        musicController.Stop();
        musicController.ChangeState(gameState);

        loadedJSON = false;
    }

    void Update()
    {
        UpdateTimer();
        if (loadJSON)
        {
            if (!loadedJSON)
            {
                mapController.LoadMap(gameType == GameType.EDITOR);
                loadedJSON = true;
            }
        }
        else if (!setIds)
        {
            mapController.AssignObjectIds();
            setIds = true;
        }

        if (gameState == GameState.SHAKE)
            Shake();

        UpdateControls();
    }

    public void SaveMap()
    {
        mapController.SaveMap(gameType == GameType.EDITOR);
    }

    void UpdateControls()
    {
        /*
        if (Input.GetKeyDown(KeyCode.H))
        {
            SaveMap();
        }*/

        if (Input.GetKeyDown(KeyCode.Keypad9))
        {
            mapController.ReloadMapContent(gameType == GameType.EDITOR);
            for (int i = 0; i < ListCollectableCtrl.Count; i++)
                hudController.UpdateNbCollectables(true);
        }
        
        if (Input.GetKeyDown(KeyCode.Keypad1))
        {
            Shake();
        }
        
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (gameType == GameType.EDITOR)
            {
                if (gameState != GameState.OBJECTSELECTOR)
                    StartObjectSelector();
                else
                    Resume();
            }
            else
            {
                if (gameState != GameState.PAUSE)
                    Pause();
                else
                    Resume();
            }
        }
    }

    void Pause()
    {
        hudController.Pause();
        musicController.Pause();

        gameStateBeforePause = gameState;
        gameState = GameState.PAUSE;
    }

    void StartObjectSelector()
    {
        gameStateBeforePause = gameState;
        gameState = GameState.OBJECTSELECTOR;
        hudController.StartObjectSelector();
    }

    public void Resume()
    {
        if (gameState == GameState.PAUSE)
            hudController.ResumeFromPause();
        else if (gameState == GameState.OBJECTSELECTOR)
            hudController.ResumeFromObjectSelector();

        musicController.UnPause();

        gameState = gameStateBeforePause;
    }

    public void Exit()
    {
        Time.timeScale = 1;
        LevelProperties.score = null;
        SceneManager.LoadScene("Menu");
    }

    void UpdateTimer()
    {
        if (gameState == GameState.INSPECT)
        {
            if (timeLeft <= 0)
            {
                if (shake)
                    Shake();
                else
                    EndShake();
            }
            else
                timeLeft -= Time.deltaTime;
        }
        else if (gameState == GameState.RESTORE)
            timeSpentResolving += Time.deltaTime;
    }

    public GameObject GetPrefab(string name)
    {
        foreach (GameObject o in prefabs)
        {
            if (o.name == name)
                return o;
        }

        throw new Exception("Could not find the prefab " + name);
    }

    private void DisablePlayer(bool shaking)
    {
        fpsController.enabled = !shaking;
        pointerController.enabled = !shaking;
    }

    void Shake()
    {
        if (gameState == GameState.SHAKE)
        {
            // Shake Over
            if (!cameraShake.isRunning)
                EndShake();
        }
        else if (gameState != GameState.PAUSE) // First call to Shake
        {
            gameState = GameState.SHAKE;
            musicController.ChangeState(gameState);

            DisablePlayer(true);

            hudController.Shake();
            cameraShake.ShakeCamera();

            foreach (var l in ListLockers)
                l.Shake();

            foreach (var collectable in ListCollectableCtrl)
                collectable.Shake();

            foreach (var s in ListSlotCtrl)
                s.Shake();

            foreach (var p in ListPerfs)
                p.GetComponentInChildren<WaterController>().Shake();
        }
    }

    void EndShake()
    {
        DisablePlayer(false);

        hudController.EndShake();

        foreach (var collectable in ListCollectableCtrl)
            collectable.EndShake();

        foreach (var s in ListSlotCtrl)
            s.EndShake();

        foreach (var npc in ListNPC)
            npc.GetComponent<NPCController>().EndShake();

        gameState = GameState.RESTORE;
        musicController.ChangeState(gameState);
    }

    // Needs to be recursive because can access from inner collider
    public int GetIdOf(GameObject o)
    {
        return GetIdOfRec(o);
    }

    private int GetIdOfRec(GameObject o)
    {
        if (o == null)
            return -1;
        var objCtrl = o.GetComponent<ObjectController>();
        if (objCtrl != null)
            return objCtrl.id;
        else
            return GetIdOfRec(o.transform.parent != null ? o.transform.parent.gameObject : null);
    }

    public GameObject GetObjectById(int id)
    {
        foreach (var o in ListObjectCtrl)
        {
            if (o.id == id)
                return o.gameObject;
        }

        return null;
    }

    public CollectableController GetCollectableById(int id)
    {
        foreach (var o in ListObjectCtrl)
        {
            var currId = o.id;
            if (currId == id)
                return o.GetComponent<CollectableController>();
        }

        return null;
    }

    public void EnableSlotsColliders(bool enable)
    {
        foreach (var s in ListSlotCtrl)
            s.EnableColliders(enable);
    }

    public void ChangeLayersToIgnore(bool ignore)
    {
        var val = ignore ? 2 : 0;
        // slots
        foreach (var slot in ListSlotCtrl)
            slot.gameObject.layer = val;

        // collectables and children of collectables
        foreach (var col in ListCollectableCtrl)
        {
            col.gameObject.layer = val;
            ChangeLayersToIgnoreRec(col.transform, val);
        }

        if (playerController.collected != null)
            ChangeLayersToIgnoreRec(playerController.collected.transform, val);
    }

    private void ChangeLayersToIgnoreRec(Transform t, int val)
    {
        t.gameObject.layer = val;
        foreach (Transform child in t.transform)
        {
            child.gameObject.layer = val;
            ChangeLayersToIgnoreRec(child, val);
        }
    }
}
