﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerfMoreButtonController : InteractionButtonController
{
    WaterController waterController;

    public override void Start()
    {
        base.Start();
        waterController = transform.parent.parent.Find("Water").GetComponent<WaterController>();
    }

    public override void DoAction()
    {
        if (timerReclick.IsOver())
            waterController.ChangeWaterLevel(true);
        base.DoAction();
    }
}
