﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureController : MonoBehaviour {
    public int currentMaterialIndex = 0;
    public List<Material> materials = new List<Material>();
}
