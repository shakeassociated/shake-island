﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class HUDController : MonoBehaviour
{
    Transform canvas, pauseTransform;
    GameObject GUI3D, objectSelector;
    Text collectText, dropText, topText, colNbText;
    public enum PointerState { COLLECT, DROP, NONE };
    GameObject guiSpot, fpsPointer, colNbIcon, modal;
    public Texture pointerNoneTexture, pointerActiveTexture;
    PointerState pointerState;
    private GameObject collectedDisplayedObject;
    GameController gameController;
    public GameObject shakerGUI;
    public ShakerController shakerController;

    public Text talkText;
    string talkingLine;
    bool isTalking;
    Coroutine talkingCoroutine;
    NPCController npcTalking;
    AudioSource currentAudioNPC;

    int nbCollectablesPlaced;

    private void Awake()
    {
        canvas = GameObject.Find("Canvas").transform;

        pauseTransform = canvas.Find("Pause");
        fpsPointer = canvas.Find("FPS Pointer").gameObject;
        var tmp = canvas.Find("ObjectSelector");
        if (tmp)
            objectSelector = tmp.gameObject;

        colNbIcon = canvas.Find("ColNbIcon").gameObject;
        modal = canvas.Find("Modal").gameObject;

        // Texts
        collectText = canvas.Find("Collect").GetComponent<Text>();
        dropText = canvas.Find("Drop").GetComponent<Text>();
        talkText = canvas.Find("TalkText").GetComponent<Text>();
        topText = canvas.Find("TopText").GetComponent<Text>();
        colNbText = canvas.Find("ColNbText").GetComponent<Text>();

        GUI3D = GameObject.Find("3D GUI");
        guiSpot = GUI3D.transform.Find("3D GUI Spot").gameObject;

        gameController = GetComponent<GameController>();
    }

    void Start()
    {
        Restart();
    }

    public void Restart()
    {
        pointerState = PointerState.NONE;
        collectedDisplayedObject = null;

        talkingLine = "";
        talkingCoroutine = null;
        npcTalking = null;
        currentAudioNPC = null;

        colNbIcon.SetActive(false);
        colNbText.gameObject.SetActive(false);
        nbCollectablesPlaced = 0;

        modal.SetActive(false);
        if (shakerGUI)
        {
            shakerGUI.SetActive(true);
            shakerController.Restart(gameController);
        }
    }

    public void ChangePointerState(PointerState newState)
    {
        pointerState = newState;

        switch (pointerState)
        {
            case PointerState.COLLECT:
                collectText.enabled = true;
                dropText.enabled = false;
                break;
            case PointerState.DROP:
                collectText.enabled = false;
                dropText.enabled = true;
                break;
            default:
                collectText.enabled = false;
                dropText.enabled = false;
                break;
        }

        EnablePointerTexture(pointerState != PointerState.NONE);
    }

    public void SetAsLevelEditor()
    {
        UpdateTopText("Mode editeur");
    }

    private void EnablePointerTexture(bool val)
    {
        fpsPointer.GetComponent<RawImage>().texture = val ? pointerActiveTexture : pointerNoneTexture;
    }

    public void DisplayCollectedObject(GameObject collected)
    {
        // Disable previous object
        StopDisplayCollectedObject();
        
        GameObject prefab = gameController.GetPrefab(collected.name);
        GameObject o = Instantiate(prefab);

        o.transform.parent = guiSpot.transform;

        Rigidbody rb = o.GetComponent<Rigidbody>();
        if (rb)
            rb.isKinematic = true; // to true normally

        ObjectController objController = o.GetComponent<ObjectController>();
        objController.CopyTexturesOf(collected);

        CollectableController newCollectableCtrl = o.GetComponent<CollectableController>();
        if (newCollectableCtrl)
        {
            newCollectableCtrl.displayOnly = true;
            if (newCollectableCtrl.scaleForDisplayOnly != Vector3.zero)
                o.transform.localScale = newCollectableCtrl.scaleForDisplayOnly;

            o.transform.localRotation = Quaternion.Euler(newCollectableCtrl.rotationForDisplayOnly);
            o.transform.localPosition = newCollectableCtrl.positionForDisplayOnly;
        }

        WaterBottleController collectedWaterBottleController = collected.GetComponent<WaterBottleController>();
        if (collectedWaterBottleController != null)
        {
            var isCollectedContentActive = collectedWaterBottleController.transform.Find("Content").gameObject.activeSelf;
            o.transform.Find("Content").gameObject.SetActive(isCollectedContentActive);
        }

        collectedDisplayedObject = o;
    }

    public void StopDisplayCollectedObject()
    {
        if (collectedDisplayedObject)
        {
            Destroy(collectedDisplayedObject);
            collectedDisplayedObject = null;
        }
    }

    public void MakeNPCTalk(NPCController newNPCTalking, string fullText, AudioSource audio)
    {
        if (isTalking)
        {
            StopCoroutine(talkingCoroutine);
            npcTalking.EnableTalkingAnimation(false);
            currentAudioNPC.Stop();
        }

        npcTalking = newNPCTalking;
        npcTalking.EnableTalkingAnimation(true);
        talkingCoroutine = StartCoroutine(AnimateText(npcTalking, fullText));
        if (audio != null)
        {
            currentAudioNPC = audio;
            currentAudioNPC.Play();
        }
    }

    IEnumerator AnimateText(NPCController npcTalking, string strComplete)
    {
        this.npcTalking = npcTalking;
        isTalking = true;

        int i = 0;
        int nbWordsPrinted = 1;
        talkingLine = "";

        while (i < strComplete.Length)
        {
            var c = strComplete[i];
            talkingLine += c;
            i++;

            if (c == ' ')
                nbWordsPrinted++;
            if (nbWordsPrinted == 7)
            {
                yield return new WaitForSeconds(0.5f);
                talkingLine = "";
                nbWordsPrinted = 0;
            }
            yield return new WaitForSeconds(0.05f);
        }
        yield return new WaitForSeconds(0.5f);

        talkingLine = "";
        isTalking = false;
        talkingCoroutine = null;
        npcTalking.EnableTalkingAnimation(false);
        npcTalking = null;
    }

    public void UpdateTopText(string text)
    {
        topText.text = text;
    }

    public void UpdateNbCollectables(bool add)
    {
        if (gameController.gameType == GameController.GameType.GAME)
        {
            nbCollectablesPlaced += add ? 1 : -1;
            nbCollectablesPlaced = nbCollectablesPlaced < 0 ? 0 : nbCollectablesPlaced;
            colNbText.text = nbCollectablesPlaced + " / " + gameController.ListCollectableCtrl.Count;
        }
    }

    public void Shake()
    {
        if (shakerGUI)
            shakerGUI.SetActive(false);

        topText.gameObject.SetActive(false);
        fpsPointer.SetActive(false);
    }

    public void EndShake()
    {
        topText.gameObject.SetActive(true);
        fpsPointer.SetActive(true);

        if (gameController.gameType == GameController.GameType.GAME)
        {
            colNbIcon.SetActive(true);
            colNbText.gameObject.SetActive(true);
            colNbText.text = nbCollectablesPlaced + " / " + gameController.ListCollectableCtrl.Count;
        }
    }

    public void Pause()
    {
        pauseTransform.gameObject.SetActive(true);

        EnableCursorAndFreezeFPS(true);
    }

    public void ResumeFromPause()
    {
        pauseTransform.gameObject.SetActive(false);

        EnableCursorAndFreezeFPS(false);
    }

    public void StartObjectSelector()
    {
        objectSelector.SetActive(true);

        EnableCursorAndFreezeFPS(true);
    }

    public void ResumeFromObjectSelector()
    {
        objectSelector.SetActive(false);

        EnableCursorAndFreezeFPS(false);
    }

    private void Update()
    {
        if (isTalking)
        {
            if (LevelProperties.subtitles)
                talkText.text = talkingLine;
        }
        else
            talkText.text = "";
    }

    public void CreateModal(string title, string message, bool inGame)
    {
        if (!modal.activeSelf)
        {
            var modalCtrl = modal.GetComponent<ModalController>();
            modalCtrl.Set(title, message, inGame);
            modal.SetActive(true);

            EnableCursorAndFreezeFPS(true);
        }
    }

    public void KillModal()
    {
        if (modal.activeSelf)
        {
            modal.SetActive(false);

            var modalCtrl = modal.GetComponent<ModalController>();
            if (modalCtrl.InGame)
                EnableCursorAndFreezeFPS(false);
        }
    }

    public void EnableCursorAndFreezeFPS(bool freezePlayer)
    {
        fpsPointer.gameObject.SetActive(!freezePlayer);
        gameController.fpsController.enabled = !freezePlayer;
        Cursor.lockState = freezePlayer ? CursorLockMode.None : CursorLockMode.Locked;
        Cursor.visible = freezePlayer;

        Time.timeScale = freezePlayer ? 0 : 1;
    }

    public void EnableCursorAndFreezeFPSWithoutTimeScale(bool freezePlayer)
    {
        fpsPointer.gameObject.SetActive(!freezePlayer);
        gameController.fpsController.enabled = !freezePlayer;
        Cursor.lockState = freezePlayer ? CursorLockMode.None : CursorLockMode.Locked;
        Cursor.visible = freezePlayer;
    }
}
