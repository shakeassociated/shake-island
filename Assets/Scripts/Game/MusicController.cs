﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour
{
    public AudioClip clipBeforeShake, clipAfterShake;
    private AudioSource audioBeforeShake, audioAfterShake;
    public AudioSource currAudioSource;

    public void Start()
    {
        audioBeforeShake = gameObject.AddComponent<AudioSource>();
        audioBeforeShake.clip = clipBeforeShake;

        audioAfterShake = gameObject.AddComponent<AudioSource>();
        audioAfterShake.clip = clipAfterShake;
        audioAfterShake.loop = true;

        currAudioSource = null;
    }

    public void ChangeState(GameController.GameState gameState)
    {
        if (gameState == GameController.GameState.INSPECT)
        {
            audioBeforeShake.Play();
            currAudioSource = audioBeforeShake;
        }
        else if (gameState == GameController.GameState.SHAKE)
        {
            audioBeforeShake.Stop();
            currAudioSource = null;
        }
        else if (gameState == GameController.GameState.RESTORE)
        {
            audioAfterShake.Play();
            currAudioSource = audioAfterShake;
        }
        if (currAudioSource != null)
            currAudioSource.volume = 0.2f;
    }

    public void Pause()
    {
        if (currAudioSource != null)
            currAudioSource.Pause();
    }

    public void UnPause()
    {
        if (currAudioSource != null)
            currAudioSource.UnPause();
    }

    public void Stop()
    {
        if (currAudioSource != null)
            currAudioSource.Stop();
    }
}
