﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelProperties {
    public static float volume = 1f;
    public static bool subtitles = true;
    public static Score score;
    public static Dictionary<string, KeyCode> keys = new Dictionary<string, KeyCode>()
    {
        { "Pause", KeyCode.P },
        { "Jump", KeyCode.Space },
        { "Up", KeyCode.Z },
        { "Left", KeyCode.Q },
        { "Down", KeyCode.S },
        { "Right", KeyCode.D },
        { "Run", KeyCode.LeftShift },
        { "Rotate left", KeyCode.Mouse2 },
        { "Rotate right", KeyCode.Mouse3 }
    };
    public static string levelToLoad = "level";
    public static LevelType levelTypeToLoad = LevelType.LOCAL;

    /* Resource     : data within the game (premade levels + tutorials)
     * Local        : levels created with the level editor (persistent data)
     * Downloaded   : levels created by other users and download via the level selector (persistent data)
     */
    public enum LevelType { RESOURCE, LOCAL, DOWNLOADED };
}
