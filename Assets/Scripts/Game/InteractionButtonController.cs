﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionButtonController : MonoBehaviour
{
    public bool floating;

    protected bool isClickable, mouseOnIt;
    protected GameObject text;
    protected MeshRenderer meshRenderer;

    // Floating
    float rangeMovement, currMovement, speed;
    bool goingUp;

    protected GameController gameController;
    protected GameController GameController
    {
        get
        {
            if (gameController == null)
                return gameController = FindObjectOfType<GameController>();
            return gameController;
        }
        set { gameController = value; }
    }

    protected CustomTimer timerReclick;
    
    public virtual void Start()
    {
        text = transform.Find("Text").gameObject;
        meshRenderer = GetComponent<MeshRenderer>();

        isClickable = false;
        EnableClickable(false);

        rangeMovement = 0.04f;
        currMovement = 0f;
        goingUp = true;
        speed = 0.09f;

        timerReclick = new CustomTimer(2);
        timerReclick.Stop();
    }

    public virtual void DoAction()
    {
        if (timerReclick.IsOver())
            timerReclick.Restart();
    }

    public virtual void Update()
    {
        if (floating)
        {
            if (isClickable)
            {
                if ((goingUp && currMovement > rangeMovement) || (!goingUp && currMovement < -rangeMovement))
                    goingUp = !goingUp;

                transform.position = new Vector3(
                    transform.position.x,
                    transform.position.y + (goingUp ? 1 : -1) * speed * Time.deltaTime,
                    transform.position.z);

                currMovement += (goingUp ? 1 : -1) * speed * Time.deltaTime;
            }

            speed = mouseOnIt ? 0f : 0.05f;
        }
        timerReclick.Update();
    }

    public void MouseOnIt(bool val)
    {
        mouseOnIt = val;
    }

    public void EnableClickable(bool val)
    {
        isClickable = val;

        text.SetActive(val);
        meshRenderer.enabled = val;
    }
}
