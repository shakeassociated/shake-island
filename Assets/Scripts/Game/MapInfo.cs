﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CollectableType { NONE, REGULAR, VERTICAL, DISC, FURNITURE_EDITOR, VERTICAL_FURNITURE_EDITOR, CEILING_FURNITURE_EDITOR, NPC_EDITOR };
public enum DisplayType { REGULAR, RADIO };

/*
 * All types of objects to load :

 * (Free) Slot : OBJ + slot ctrl
    -> MapSlot

 * Mobilier editor : OBJ + col + col type == FURNITURE_EDITOR
    -> MapFurniture avec List<MapSlot>

 * NPC editor : OBJ + col + col type == NPC_EDITOR
    -> MapNPC avec editor = true

 * Collectables : OBJ + col
    -> MapCollectable

 * Mobiliers sans slots et objets random : OBJ
    -> MapObject

 * NPC : npc controller
    -> MapNPC
 */

[System.Serializable]
public class ObjectTexture
{
    public string objectName;
    public int currentTextureIndex;

    public ObjectTexture(string objectName, int currentTextureIndex)
    {
        this.objectName = objectName;
        this.currentTextureIndex = currentTextureIndex;
    }
}

[System.Serializable]
public class MapObject
{
    public int id;
    public string prefabName;
    public Vector3 position, localScale, rotation;
    public List<ObjectTexture> objectsTexture;

    /// <summary>
    /// For random objects
    /// </summary>
    /// <param name="id"></param>
    /// <param name="prefabName"></param>
    /// <param name="t"></param>
    public MapObject(int id, string prefabName, Transform t, List<ObjectTexture> objectsTexture)
    {
        this.id = id;
        this.prefabName = prefabName;
        position = t.position;
        localScale = t.lossyScale;
        rotation = new Vector3(t.eulerAngles.x, t.eulerAngles.y, t.eulerAngles.z);
        this.objectsTexture = objectsTexture;
    }
}

[System.Serializable]
public class MapSlot
{
    public int id;
    public DisplayType displayType;
    public string prefabName;
    public Vector3 position, localScale, rotation;
    public bool checkRotation;
    public int idExpected;

    /// <summary>
    /// For slots, precise the idExpected (0 for nothing)
    /// </summary>
    /// <param name="id"></param>
    /// <param name="prefabName"></param>
    /// <param name="t"></param>
    /// <param name="idExpected"></param>
    public MapSlot(int id, string prefabName, Transform t, List<ObjectTexture> objectsTexture, SlotController slotController)
    {
        this.id = id;
        this.prefabName = prefabName;
        position = t.position;
        localScale = t.localScale;
        rotation = new Vector3(t.eulerAngles.x, t.eulerAngles.y, t.eulerAngles.z);
        displayType = slotController.displayType;
        checkRotation = false;

        idExpected = slotController.idOnIt;
    }
}

// EDITOR Furniture
[System.Serializable]
public class MapFurniture
{
    public int id;
    public string prefabName;
    public Vector3 position, localScale, rotation;
    public List<ObjectTexture> objectsTexture;
    public List<MapSlot> innerSlots;

    public MapFurniture(int id, string prefabName, Transform t, List<ObjectTexture> objectsTexture, List<MapSlot> innerSlots)
    {
        this.id = id;
        this.prefabName = prefabName;
        position = t.position;
        localScale = t.lossyScale;
        rotation = new Vector3(t.eulerAngles.x, t.eulerAngles.y, t.eulerAngles.z);
        this.objectsTexture = objectsTexture;
        
        this.innerSlots = innerSlots;
    }
}

[System.Serializable]
public class MapCollectable
{
    public int id;
    public DisplayType displayType;
    public string prefabName;
    public Vector3 position, localScale, rotation;
    public List<ObjectTexture> objectsTexture;
    public bool checkRotation;
    public bool auditive;
    public int slotIdExpected;

    /// <summary>
    /// For collectables, precise the checkrotation
    /// </summary>
    /// <param name="id"></param>
    /// <param name="prefabName"></param>
    /// <param name="t"></param>
    /// <param name="checkRotation"></param>
    public MapCollectable(int id, string prefabName, Transform t, List<ObjectTexture> objectsTexture, CollectableController colController)
    {
        this.id = id;
        this.prefabName = prefabName;
        position = t.position;
        localScale = t.lossyScale;
        rotation = new Vector3(t.eulerAngles.x, t.eulerAngles.y, t.eulerAngles.z);
        displayType = colController.displayType;
        this.objectsTexture = objectsTexture;
        checkRotation = colController.checkRotation;
        auditive = colController.auditive;
        slotIdExpected = colController.slotId;
    }
}

[System.Serializable]
public class PlayerInfo
{
    public Vector3 position;

    public PlayerInfo(Vector3 position)
    {
        this.position = position;
    }
}

[System.Serializable]
public class ShakeCameraInfo
{
    public Vector3 position, rotation;

    public ShakeCameraInfo(Transform t)
    {
        position = t.position;
        rotation = new Vector3(t.eulerAngles.x, t.eulerAngles.y, t.eulerAngles.z);
    }
}

[System.Serializable]
public class MapNPC
{
    public int id;
    public string prefabName, talkText;
    public Vector3 position, localScale, rotation;
    public bool buttonFinish, buttonTalk;
    public bool editor;

    public MapNPC(int id, string prefabName, Transform t, bool buttonFinish, string talkText, bool editor)
    {
        this.id = id;
        this.prefabName = prefabName;

        position = t.position;
        localScale = t.lossyScale;
        rotation = new Vector3(t.eulerAngles.x, t.eulerAngles.y, t.eulerAngles.z);

        this.buttonFinish = buttonFinish;
        buttonTalk = talkText != "";
        this.talkText = talkText;

        this.editor = editor;
    }
}

[System.Serializable]
public class MapPerf
{
    public int id;
    public string prefabName;
    public Vector3 position, localScale, rotation;
    public int waterLevel;

    public MapPerf(int id, Transform t, int waterLevel)
    {
        this.id = id;
        prefabName = "Perf";

        position = t.position;
        localScale = t.lossyScale;
        rotation = new Vector3(t.eulerAngles.x, t.eulerAngles.y, t.eulerAngles.z);

        this.waterLevel = waterLevel;
    }
}

[System.Serializable]
public class MapInfo
{
    public PlayerInfo playerInfo;
    public ShakeCameraInfo shakeCameraInfo;
    public List<MapObject> mapObjects;
    public List<MapSlot> mapSlots; // Only non-inner slots
    public List<MapFurniture> mapFurnitures;
    public List<MapCollectable> mapCollectables;
    public List<MapNPC> npcs;
    public List<MapPerf> perfs;
    public string mapName, tutorialData;
    public bool isTutorial;

    public void Init()
    {
        mapObjects = new List<MapObject>();
        mapSlots = new List<MapSlot>();
        mapFurnitures = new List<MapFurniture>();
        mapCollectables = new List<MapCollectable>();
        npcs = new List<MapNPC>();
        perfs = new List<MapPerf>();
    }

    public void Clear()
    {
        mapObjects.Clear();
        mapSlots.Clear();
        mapFurnitures.Clear();
        mapCollectables.Clear();
        npcs.Clear();
        perfs.Clear();
    }

    public void SetName(string name)
    {
        mapName = name;
    }

    public void SetPlayerInfo(Vector3 pos)
    {
        playerInfo = new PlayerInfo(pos);
    }

    public void SetShakeCameraInfo(Transform t)
    {
        shakeCameraInfo = new ShakeCameraInfo(t);
    }

    public void AddMapObject(MapObject mapObject)
    {
        mapObjects.Add(mapObject);
    }

    public void AddMapSlot(MapSlot mapSlot)
    {
        mapSlots.Add(mapSlot);
    }

    public void AddMapFurniture(MapFurniture mapFurniture)
    {
        mapFurnitures.Add(mapFurniture);
    }

    public void AddMapCollectable(MapCollectable mapCollectable)
    {
        mapCollectables.Add(mapCollectable);
    }

    public void AddNPC(MapNPC npc)
    {
        npcs.Add(npc);
    }
    
    public void AddPerf(MapPerf perf)
    {
        perfs.Add(perf);
    }

    public string Serialize()
    {
        return JsonUtility.ToJson(this);
    }
}
