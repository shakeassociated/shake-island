﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionBoxController : MonoBehaviour
{
    List<InteractionButtonController> buttons;
    List<GameObject> labels;
    public bool onlyInRestoreMode;

    GameController gameController;

    void Start()
    {
        gameController = FindObjectOfType<GameController>();

        buttons = new List<InteractionButtonController>();
        var childButtons = transform.parent.Find("Buttons");
        if (childButtons)
        {
            foreach (Transform b in childButtons)
                buttons.Add(b.GetComponent<InteractionButtonController>());
        }

        labels = new List<GameObject>();
        var childLabels = transform.parent.Find("Labels");
        if (childLabels)
        {
            foreach (Transform l in childLabels)
                labels.Add(l.gameObject);
        }

        // Useful, otherwise it doesn't work the first time when loaded
        GetComponent<BoxCollider>().enabled = false;
        GetComponent<BoxCollider>().enabled = true;

        MeshRenderer mr = GetComponent<MeshRenderer>();
        if (gameController)
            mr.enabled = gameController.gameType == GameController.GameType.EDITOR;
    }

    void SetButtonsActive(bool val)
    {
        foreach (var b in buttons)
        {
            if (b.gameObject.activeSelf)
                b.EnableClickable(val);
        }
    }

    void SetLabelsActive(bool val)
    {
        foreach (var l in labels)
            l.SetActive(val);
    }

    private bool CorrectGameState()
    {
        return !onlyInRestoreMode
            || gameController.gameState == GameController.GameState.RESTORE
            || gameController.gameType == GameController.GameType.EDITOR;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (CorrectGameState() && other.name == "Player")
        {
            SetButtonsActive(true);
            SetLabelsActive(true);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (CorrectGameState() && other.name == "Player")
        {
            SetButtonsActive(true);
            SetLabelsActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (CorrectGameState() && other.name == "Player")
        {
            SetButtonsActive(false);
            SetLabelsActive(false);
        }
    }
}
