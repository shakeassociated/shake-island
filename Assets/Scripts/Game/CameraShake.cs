﻿using UnityEngine;
using System.Collections;

///Shakes camera parent object
public class CameraShake : MonoBehaviour
{
    public float shakeAmount, shakeDuration;
    float startAmount, startDuration, shakePercentage;

    public bool isRunning;

    GameObject player;
    Camera cameraPlayer, cameraWhenShake;

    AudioSource audiosource;

    private void Awake()
    {
        audiosource = GetComponent<AudioSource>();
    }

    void Start()
    {
        player = GameObject.Find("Player");
        cameraPlayer = player.transform.Find("FirstPersonCharacter").GetComponent<Camera>();
        cameraWhenShake = GameObject.Find("CameraWhenShake").GetComponent<Camera>();

        isRunning = false;
    }

    public void ShakeCamera()
    {
        startAmount = 10f;
        shakeAmount = startAmount;

        startDuration = 5f;
        shakeDuration = startDuration;

        if (!isRunning)
            StartCoroutine(Shake());
    }

    public void ShakeCamera(float amount, float duration)
    {
        startAmount = amount;
        shakeAmount = startAmount;

        startDuration = duration;
        shakeDuration = startDuration;

        if (!isRunning)
            StartCoroutine(Shake());
    }

    private void SwipeCamera(bool shakeCamera)
    {
        cameraWhenShake.enabled = shakeCamera;
        cameraPlayer.enabled = !shakeCamera;
    }
    
    IEnumerator Shake()
    {
        isRunning = true;
        SwipeCamera(true);
        audiosource.PlayOneShot(audiosource.clip);

        while (shakeDuration > 0.01f)
        {
            Vector3 rotationAmount = Random.insideUnitSphere * shakeAmount;
            rotationAmount.z = 0;
            transform.localRotation = Quaternion.Euler(rotationAmount);

            /* Reduce shake amout and duration */
            shakePercentage = shakeDuration / startDuration;
            shakeAmount = startAmount * shakePercentage; // Less and less shaking

            shakeDuration -= Time.deltaTime;
            yield return null;
        }
        transform.localRotation = Quaternion.identity;

        SwipeCamera(false);
        isRunning = false;
    }
}