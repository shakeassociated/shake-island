﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlotController : MonoBehaviour
{
    ObjectController objController;
    public int idExpected, idOnIt;
    public bool alreadySet, isObjectTurning;
    GameObject turningObject;
    CollectableController collectableCtrl;
    List<MeshRenderer> meshRenderers;
    private bool printRenderer;
    public bool displayObjectOnIt, correctlyTurned;
    public CollectableType collectableExpected;
    public DisplayType displayType;

    private bool isAnInnerSlot;
    public bool IsAnInnerSlot
    {
        get { return isAnInnerSlot; }
    }

    void Start()
    {
        objController = GetComponent<ObjectController>();
        collectableCtrl = null;
        meshRenderers = new List<MeshRenderer>();
        var mr = GetComponent<MeshRenderer>();
        if (mr)
            meshRenderers.Add(mr);
        var mrChildren = GetComponentsInChildren<MeshRenderer>();
        foreach (var m in mrChildren) 
            meshRenderers.Add(m);
        EnableMeshRenderers(false);

        isObjectTurning = false;
        printRenderer = false;
        displayObjectOnIt = true;

        switch (gameObject.name)
        {
            case "Slot":
                collectableExpected = CollectableType.REGULAR;
                break;

            case "SlotV":
                collectableExpected = CollectableType.VERTICAL;
                break;

            case "SlotBox":
                collectableExpected = CollectableType.DISC;
                break;

            default:
                collectableExpected = CollectableType.NONE;
                break;
        }

        isAnInnerSlot = IsInnerSlot();
        correctlyTurned = false;
    }

    private void EnableMeshRenderers(bool val)
    {
        foreach (var m in meshRenderers)
            m.enabled = val;
    }

    private void Update()
    {
        if (isObjectTurning && collectableExpected != CollectableType.NONE)
        {
            Transform t = turningObject.transform;
            
            float scrollwheel = Input.GetAxis("Mouse ScrollWheel");
            Vector3 axisRotation = new Vector3();
            float valueRotation = 0f;

            if (collectableExpected == CollectableType.VERTICAL)
            {
                // Setting to correct first rotation
                if (t.rotation.eulerAngles.y != transform.rotation.eulerAngles.y)
                    t.eulerAngles = new Vector3(t.eulerAngles.x, transform.eulerAngles.y, t.eulerAngles.z);
                correctlyTurned = true;

                axisRotation = new Vector3(0, 0, 1);
                valueRotation = 180f;
            }
            else if (collectableExpected == CollectableType.REGULAR)
            {
                axisRotation = new Vector3(0, 1, 0);
                valueRotation = 90 * (scrollwheel > 0f ? -1 : 1);
                correctlyTurned = true;
            }
            else if (collectableExpected == CollectableType.DISC)
            {
                // Set to correct angle by checking the difference is either 0° or 180°
                var diffAngle = Quaternion.Angle(Quaternion.Euler(transform.eulerAngles), Quaternion.Euler(t.eulerAngles));
                if (!(diffAngle > 0 && diffAngle < 1) && !(diffAngle > 179 && diffAngle < 181))
                {
                    var eulerAngles = t.rotation.eulerAngles;
                    eulerAngles.y = transform.rotation.eulerAngles.y;
                    t.eulerAngles = eulerAngles;
                }
                correctlyTurned = true;

                axisRotation = new Vector3(0, 1, 0);
                valueRotation = 180 * (scrollwheel > 0f ? -1 : 1);
            }

            if (collectableCtrl.checkRotation && scrollwheel != 0f)
                t.Rotate(axisRotation, valueRotation);
        }
        EnableMeshRenderers(printRenderer || isObjectTurning);
    }

    public void TurnObjectOnTop(GameObject objectToTurn)
    {
        if (!isObjectTurning)
        {
            turningObject = objectToTurn;
            collectableCtrl = turningObject.GetComponent<CollectableController>();
            collectableCtrl.DisplayRenderer(true);

            turningObject.transform.parent = null;
            turningObject.transform.localPosition = Vector3.zero;
            turningObject.transform.position = transform.position;

            isObjectTurning = true;

            DisplayObjectOnIt(false);
        }
    }

    public void StopTurningObject()
    {
        if (isObjectTurning)
        {
            isObjectTurning = false;

            DisplayObjectOnIt(true);

            collectableCtrl = null;

            correctlyTurned = false;
        }
    }

    public void PrintRenderer(bool val)
    {
        printRenderer = val;
    }

    public void UnlinkCollectable()
    {
        idOnIt = 0;
        foreach (BoxCollider b in GetComponents<BoxCollider>())
            b.enabled = true;
    }

    public GameObject GetObjectOnIt()
    {
        if (idOnIt > 0)
            return objController.GameController.GetObjectById(idOnIt);
        return null;
    }

    public void DisplayObjectOnIt(bool display)
    {
        var obj = GetObjectOnIt();
        if (obj)
        {
            var col = obj.GetComponent<CollectableController>();
            if (col)
            {
                col.DisplayRenderer(display);
            }
        }
        displayObjectOnIt = display;
    }

    public void Shake()
    {
        UnlinkCollectable();
        EnableColliders(false);
    }

    public void EndShake()
    {
        EnableColliders(false);
    }

    public void EnableColliders(bool enable)
    {
        var boxColliders = GetComponents<BoxCollider>();
        foreach (var b in boxColliders)
            b.enabled = enable;

        var meshColliders = GetComponents<MeshCollider>();
        foreach (var m in meshColliders)
            m.enabled = enable;
    }

    private bool IsInnerSlot()
    {
        var curr = transform.parent;
        while (curr != null && curr.GetComponent<CollectableController>() == null)
            curr = curr.transform.parent;

        return curr != null;
    }

    public bool CorrectObjectOnIt()
    {
        if (idExpected == idOnIt)
            return true;

        if ((idExpected == 0 && idOnIt != 0) || (idExpected != 0 && idOnIt == 0))
            return false;

        var objectCtrlRef = objController.GameController.GetObjectById(idExpected).GetComponent<ObjectController>();
        var objectOnItController = objController.GameController.GetObjectById(idOnIt).GetComponent<ObjectController>();
        return objectOnItController.IsTheSameAs(objectCtrlRef);
    }
}
