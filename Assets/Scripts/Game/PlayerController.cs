﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerController : MonoBehaviour
{
    public GameController gameController;
    public GameObject collected { get; set; }
    CollectableController colController;
    private Vector3 originPos;

    public Vector3 OriginPos
    {
        get { return originPos; }
    }

    void Start()
    {
        collected = null;
        gameController = FindObjectOfType<GameController>();
        originPos = transform.position;
    }

    public void Collect(GameObject collected)
    {
        this.collected = collected;
        colController = collected.GetComponent<CollectableController>();
    }

    public void Drop(SlotController slotController)
    {
        if (collected != null)
        {
            colController.Drop(slotController);
            collected = null;
        }
    }

    public void Drop(Vector3 newPosition)
    {
        if (collected != null)
        {
            colController.Drop(newPosition);
            collected = null;
        }
    }

    public void Throw()
    {
        if (collected != null)
        {
            colController.Throw();
            collected = null;
        }
    }

    public bool HasACollected()
    {
        return collected != null;
    }

    public void DeleteCollected()
    {
        if (collected != null)
        {
            colController.Delete();
            collected = null;
        }
    }
}
