﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSelectorController : MonoBehaviour {
    public GameObject canvasObjectSelector;
    public Texture2D mousePointer;

    private void Start()
    {
    }

    public void Init()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        Cursor.SetCursor(mousePointer, Vector2.zero, CursorMode.Auto);
    }

    public void Update()
    {

    }
}
