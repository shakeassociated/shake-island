﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkerController : MonoBehaviour
{
    Transform origin, end;
    CustomTimer timer;
    bool isTurning;

    private void Awake()
    {
        origin = transform.parent.Find("Origin");
        end = transform.parent.Find("End");
    }

    private void Start()
    {
        timer = new CustomTimer(6f);
        timer.Stop();
        isTurning = false;
    }

    void Update()
    {
        if (!isTurning)
            transform.Translate(Vector3.forward * Time.deltaTime * 0.54f);
        timer.Update();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (timer.IsOver())
        {
            if (other.name == "End" || other.name == "Origin")
                StartCoroutine(TurnAround());
        }
    }

    private IEnumerator TurnAround()
    {
        timer.Restart();
        isTurning = true;
        
        Quaternion beginRotation = transform.rotation;
        var yBefore = transform.rotation.eulerAngles.y;

        while (Quaternion.Angle(beginRotation, transform.rotation) < 178)
        {
            transform.Rotate(0, 40 * Time.deltaTime, 0, Space.Self);
            yield return null;
        }

        Vector3 v = transform.rotation.eulerAngles;
        transform.rotation = Quaternion.Euler(v.x, yBefore + 180, v.z);

        isTurning = false;
    }
}
