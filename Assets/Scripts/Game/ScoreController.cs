﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

/*
 * Toutes les données de score :
 * 
 * nb collectables
 * nb collectables bien placés
 * nb collectables bien tournés

 * nb perfs
 * nb perfs bien mises

 * temps passé à résoudre le niveau
 * temps passé avec un collectable régulier dans la main
 * temps passé avec un collectable vertical dans la main
 * temps passé avec un collectable auditif  dans la main
 * 
 * */
[System.Serializable]
public class Score
{
    #region Collectables
    public int nbCollectables;
    public void SetNbCollectables(int nbCollectables)
    {
        this.nbCollectables = nbCollectables;
    }

    public float timerCollectablesRegular;
    public float nbReg, nbRegCorrect;
    public void SetRegularCollectables(float nbRegCorrect, float nbReg)
    {
        this.nbRegCorrect += nbRegCorrect;
        this.nbReg += nbReg;
    }

    public float timerCollectablesVertical;
    public float nbVert, nbVertCorrect;
    public void SetVerticalCollectables(float nbVertCorrect, float nbVert)
    {
        this.nbVertCorrect += nbVertCorrect;
        this.nbVert += nbVert;
    }

    public float timerCollectablesAuditive;
    public float nbAuditive, nbAuditiveCorrect;
    public void SetAuditiveCollectables(float nbAuditiveCorrect, float nbAuditive)
    {
        this.nbAuditiveCorrect += nbAuditiveCorrect;
        this.nbAuditive += nbAuditive;
    }

    public void SetTimerCollectables(float timerRegular, float timerVertical, float timerAuditive)
    {
        timerCollectablesRegular = timerRegular;
        timerCollectablesVertical = timerVertical;
        timerCollectablesAuditive = timerAuditive;
    }
    #endregion

    #region Perfs
    public int nbPerfs, nbCorrectPerfs;

    // Perfs are auditive objects
    public void SetPerfs(int nbPerfs, int nbCorrectPerfs)
    {
        this.nbPerfs = nbPerfs;
        nbAuditive += nbPerfs;
        this.nbCorrectPerfs = nbCorrectPerfs;
        nbAuditiveCorrect += nbCorrectPerfs;
    }
    #endregion

    private float ComputeNoteCollectablesAndPerfs()
    {
        var nbObjects = nbCollectables + nbPerfs;
        float tempNote = (nbRegCorrect + nbVertCorrect + nbAuditiveCorrect) / nbObjects;

        /*Print("Beginning score compute");
        Print("reg : " + nbRegCorrect + "/" + nbReg);
        Print("vert : " + nbVertCorrect + "/" + nbVert);
        Print("aud : " + nbAuditiveCorrect + "/" + nbAuditive);
        Print("nb objects : " + nbObjects);
        Print("tempNote : " + tempNote);*/

        return tempNote * 100;
    }

    void Print(string str)
    {
        Debug.Log(str);
    }

    #region Time Spent Resolving
    public float timeSpentResolving;

    public void SetTimeSpentResolving(float timeSpentResolving)
    {
        this.timeSpentResolving = timeSpentResolving;
    }

    private float ComputeNoteTime()
    {
        int minutes = (int)timeSpentResolving / 60;
        float res = 0;

        if (minutes > 1)
        {
            res = (minutes - 1) * 20;
            res = res > 100 ? 100 : res; 
        }

        return res;
    }

    public string TotalTimeToString()
    {
        int min = (int)timeSpentResolving / 60;
        int sec = (int)timeSpentResolving % 60;
        return (min < 10 ? ("0" + min.ToString()) : min.ToString()) + " : " + (sec < 10 ? ("0" + sec.ToString()) : sec.ToString());
    }

    private float ApplyTimeToNotePlaced(float notePlaced)
    {
        float secondesRef = 120;
        float currT = 1.3f * secondesRef;
        while (timeSpentResolving > currT && currT < 2.7 * secondesRef)
        {
            notePlaced -= notePlaced * 0.15f;
            currT += 0.35f * secondesRef;
        }
        return notePlaced;
    }
    #endregion

    /// <summary>
    /// Returns a percentage of success.
    /// </summary>
    /// <returns></returns>
    public float notePercentage;
    public float ComputeNote()
    {
        notePercentage = ApplyTimeToNotePlaced(ComputeNoteCollectablesAndPerfs());
        return notePercentage;
    }

    private int GetIndexOfMinPositive(float[] arr)
    {
        int index = -1;
        float currMin = -1;
        for (int i = 0; i < arr.Length; i++)
        {
            var currVal = arr[i];
            if (currVal >= 0 && (currMin == -1 || currVal < currMin))
            {
                currMin = currVal;
                index = i;
            }
        }
        return index;
    }

    public string feedback;
    public string ComputeFeedBack()
    {
        var percReg = nbReg > 0 ? nbRegCorrect / nbReg : -1;
        var percVert = nbVert > 0 ? nbVertCorrect / nbVert : -1;
        var percAudi = nbAuditive > 0 ? nbAuditiveCorrect / nbAuditive : -1;

        float[] percentages = {
            percReg,
            percVert,
            percAudi };

        string[] feedbacks = {
            "ta vision dans l'espace",
            "l'emplacement des objects verticaux",
            "ce que l'on te dit à l'oral"
        };

        var indexOfMin = GetIndexOfMinPositive(percentages);

        if (indexOfMin == -1)
            return "Error computing the feedback.";

        string res = notePercentage < 70 ? "Tu feras mieux les prochaines fois" : "C'est du très bon travail";

        if (percentages[indexOfMin] < 0.8f)
            res += ", mais ta zone d'effort est " + feedbacks[indexOfMin] + ".";
        else if (percentages[indexOfMin] == 1)
            res += " !";
        else
            res += " ! Un conseil : donne plus d'importance à " + feedbacks[indexOfMin] + " !";

        feedback = res;
        return res;
    }

    public void Reset()
    {
        nbCollectables = 0;

        nbCorrectPerfs = 0;
        nbPerfs = 0;

        nbReg = 0;
        nbRegCorrect = 0;

        nbVert = 0;
        nbVertCorrect = 0;

        nbAuditive = 0;
        nbAuditiveCorrect = 0;
    }

    public override string ToString()
    {
        var computedScore = ComputeNote();

        return "Computed Score : " + computedScore + Environment.NewLine
            + "Time on regular collectables : " + timerCollectablesRegular + Environment.NewLine
            + "Time on vertical collectables : " + timerCollectablesVertical;
    }
}

[Serializable]
public class Scores
{
    public string levelName;
    public List<Score> scores;

    public Scores(string levelName)
    {
        this.levelName = levelName;
        scores = new List<Score>();
    }
    
    public void AddScore(Score score)
    {
        scores.Add(score);
    }

    public string Serialize()
    {
        return JsonUtility.ToJson(this);
    }
}

public class ScoreController : MonoBehaviour
{
    Score score;
    GameController gameController;
    public float timerCollectablesRegular, timerCollectablesVertical, timerCollectablesAuditive;

    private void Start()
    {
        gameController = GetComponent<GameController>();

        timerCollectablesRegular = 0f;
        timerCollectablesVertical = 0f;
        timerCollectablesAuditive = 0f;
    }
    
    public Score Analyse(bool save)
    {
        score = new Score();
        score.Reset();

        SetCollectables();
        SetPerfs();
        SetTimeSpentResolving();
        UpdateTimersCollectablesInScore();

        float res = score.ComputeNote();
        var feedback = score.ComputeFeedBack();
        print("Score final : " + res + "%");
        print(feedback);

        if (save)
            Save();

        return score;
    }

    void Save()
    {
        var mapName = gameController.mapController.mapName;
        var levelType = LevelProperties.levelTypeToLoad;

        var filePath = Application.persistentDataPath + "/Scores/";
        switch (levelType)
        {
            case LevelProperties.LevelType.RESOURCE:
                filePath += "Native/";
                break;
            case LevelProperties.LevelType.DOWNLOADED:
                filePath += "Downloaded/";
                break;
            case LevelProperties.LevelType.LOCAL:
                filePath += "Local/";
                break;
        }
        filePath += mapName + ".txt";

        Scores scores = null;
        if (File.Exists(filePath))
        {
            string previousScoresJson = File.ReadAllText(filePath);
            scores = JsonUtility.FromJson<Scores>(previousScoresJson);
        }
        else
        {
            FileInfo file = new FileInfo(filePath);
            file.Directory.Create();
            scores = new Scores(mapName);
        }
        scores.AddScore(score);

        File.WriteAllText(filePath, scores.Serialize());

        Debug.Log("Saved scores for " + "\"" + mapName + "\"");
    }
    
    void SetCollectables()
    {
        // Counts all active collectables
        float nbRegular = gameController.nbCollectablesRegular, nbCorrectRegular = 0,
            nbVertical = gameController.nbCollectablesVertical, nbCorrectVertical = 0,
            nbAuditive = gameController.nbCollectablesAuditive, nbCorrectAuditive = 0;

        foreach (var s in gameController.ListSlotCtrl)
        {
            if (s.CorrectObjectOnIt())
            {
                var colController = gameController.GetCollectableById(s.idOnIt);
                if (colController != null)
                {
                    var currVal = 0.5f;
                    if (colController.isAngleCorrect())
                        currVal = 1;

                    if (colController.auditive)
                        nbCorrectAuditive += currVal;
                    else if (colController.collectableType == CollectableType.REGULAR || colController.collectableType == CollectableType.DISC)
                        nbCorrectRegular += currVal;
                    else if (colController.collectableType == CollectableType.VERTICAL)
                        nbCorrectVertical += currVal;
                }

            }
        }

        score.SetRegularCollectables(nbCorrectRegular, nbRegular);
        score.SetVerticalCollectables(nbCorrectVertical, nbVertical);
        score.SetAuditiveCollectables(nbCorrectAuditive, nbAuditive);
        score.SetNbCollectables(gameController.ListCollectableCtrl.Count);
    }

    void SetPerfs()
    {
        int nbPerfs = gameController.ListPerfs.Count;

        int nbCorrectPerfs = 0;
        foreach (var perf in gameController.ListPerfs)
        {
            var waterController = perf.GetComponentInChildren<WaterController>();
            nbCorrectPerfs += waterController.CorrectWaterLevel() ? 1 : 0;
        }

        score.SetPerfs(nbPerfs, nbCorrectPerfs);
    }

    void SetTimeSpentResolving()
    {
        score.SetTimeSpentResolving(gameController.timeSpentResolving);
    }

    void UpdateTimersCollectablesInScore()
    {
        score.SetTimerCollectables(timerCollectablesRegular, timerCollectablesVertical, timerCollectablesAuditive);
    }
}
