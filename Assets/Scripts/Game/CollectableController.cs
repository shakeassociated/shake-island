﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableController : MonoBehaviour
{
    ObjectController objController;
    public bool taken;
    private Quaternion originalRotation;
    public int slotId, slotIdExpected;
    public bool checkRotation, displayOnly, auditive, thrown;
    public Vector3 scaleForDisplayOnly, rotationForDisplayOnly, positionForDisplayOnly;
    public CollectableType collectableType;
    public DisplayType displayType;
    Rigidbody rb;
    public List<Material> childMaterials;
    Color originalColor;
    public bool isObjectInHand, canBePlaced, hasInnerSlots;
    Behaviour halo;
    WaterBottleController waterBottleController;

    private void Awake()
    {
        objController = GetComponent<ObjectController>();
        rb = GetComponent<Rigidbody>();

        childMaterials = new List<Material>();
        if (GetComponent("Halo"))
            halo = (Behaviour)GetComponent("Halo");
        else
            halo = null;
        waterBottleController = GetComponent<WaterBottleController>();
    }

    void Start()
    {
        originalRotation = transform.rotation;

        // Black for radios
        if (displayOnly && displayType == DisplayType.RADIO)
            ApplyDisplay(false);
        rb.isKinematic = true;
        canBePlaced = true;
        thrown = false;

        hasInnerSlots = HasInnerSlots();
    }

    private void FixedUpdate()
    {
        canBePlaced = true;
        if (halo && IsEditorType())
            halo.enabled = false;
        rb.useGravity = !rb.useGravity;
    }

    private void Update()
    {
        if (displayOnly)
            transform.Rotate(new Vector3(0, 1, 0), 50 * Time.deltaTime, Space.World);
        else if (isTaken() && IsEditorRotatableType())
        {
            float scrollwheel = Input.GetAxis("Mouse ScrollWheel");
            var axisRotation = new Vector3(0, 1, 0);

            // If it has inner slots, only 90° rotation, otherwise 30°
            var valueRotation = (hasInnerSlots ? 90 : 30) * (scrollwheel > 0f ? -1 : 1);
            if (scrollwheel != 0f)
                transform.Rotate(axisRotation, valueRotation);
        }
    }

    public void Take()
    {
        taken = true;

        // For object in hand
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        DisplayRenderer(false);

        // Disable gravity
        GetComponent<Rigidbody>().isKinematic = true;

        EnableBoxColliders(IsEditorType());
        SetIsTrigger(true);

        // Remove the connexion between the slot and the object
        foreach (var slot in objController.GameController.ListSlotCtrl)
        {
            if (slot.idOnIt == objController.GameController.GetIdOf(gameObject))
            {
                slot.idOnIt = 0;
                slotId = 0;

                // Re-enables box colliders of the slot
                foreach (BoxCollider b in slot.GetComponents<BoxCollider>())
                    b.enabled = true;
            }
        }

        isObjectInHand = true;

        if (!IsEditorType())
            ChangeShader(false);
    }

    private void ChangeShader(bool toOutlined)
    {
        ChangeShaderRec(gameObject, toOutlined);
    }

    private void ChangeShaderRec(GameObject o, bool toOutlined)
    {
        var renderer = o.GetComponent<Renderer>();
        if (renderer)
        {
            var materials = renderer.materials;
            foreach (var mat in materials)
            {
                if (mat.shader.name.Equals(toOutlined ? "Standard" : "Standard (Outlined)"))
                {
                    mat.shader = Shader.Find(toOutlined ? "Standard (Outlined)" : "Standard");
                    if (!toOutlined)
                    {
                        StandardShaderUtils.ChangeRenderMode(mat, StandardShaderUtils.BlendMode.Opaque);
                    }
                    else
                    {
                        mat.SetColor("_OutlineColor", new Color(255, 153, 0));
                        mat.SetFloat("_Outline", 0.05f);
                    }
                }
            }
        }

        foreach (Transform children in o.transform)
            ChangeShaderRec(children.gameObject, toOutlined);
    }

    public void Drop(SlotController slotController)
    {
        transform.parent = null;

        transform.localPosition = Vector3.zero;
        transform.position = slotController.transform.position;

        slotController.idOnIt = objController.GameController.GetIdOf(gameObject);
        slotId = slotController.GetComponent<ObjectController>().id;

        // Enables colliders of the collectable
        EnableBoxColliders(true);
        SetIsTrigger(false);

        // Disables box colliders of the slot (for intersection)
        slotController.EnableColliders(false);
        taken = false;
        
        DisplayRenderer(true);
        rb.isKinematic = true;

        slotController.DisplayObjectOnIt(true);

        isObjectInHand = false;
    }

    public void Drop(Vector3 newPosition)
    {
        transform.parent = null;

        transform.localPosition = Vector3.zero;
        transform.position = newPosition;

        // Enables colliders of the collectable
        EnableBoxColliders(true);
        SetIsTrigger(false);

        // Disables box colliders of the slot (for intersection)
        taken = false;

        DisplayRenderer(true);
        rb.isKinematic = true;

        isObjectInHand = false;
    }

    public void DisplayRenderer(bool val)
    {
        Renderer r = GetComponent<Renderer>();
        if (r != null)
            r.enabled = val;
        DisplayRendererRec(gameObject, val);
    }

    private void DisplayRendererRec(GameObject o, bool val)
    {
        foreach (Transform child in o.transform)
        {
            Renderer rChild = child.gameObject.GetComponent<Renderer>();

            if (rChild != null)
                rChild.enabled = val;
            DisplayRendererRec(child.gameObject, val);
        }

    }

    public bool isTaken()
    {
        return taken;
    }

    public void Shake()
    {
        if (name == "Snack")
            Throw();
        else
        {
            rb.isKinematic = false;
            rb.mass = 1.5f;
            if (collectableType == CollectableType.DISC)
                rb.AddForce(transform.forward * Random.Range(1000, 1300));
            else
                rb.AddForce(Random.insideUnitSphere * 3000f);
            ApplyDisplay(false);

            // No more binded to a slot
            slotId = 0;

            if (waterBottleController != null)
                waterBottleController.Shake();
        }
    }

    public void EndShake()
    {
        rb.isKinematic = false;
        rb.mass = 50;

        ChangeShader(true);
    }

    public bool isAngleCorrect()
    {
        return checkRotation ? (int)Quaternion.Angle(transform.rotation, originalRotation) == 0 : true;
    }

    public bool isSlotCorrect()
    {
        return slotId == slotIdExpected;
    }

    public void UpdateChildMaterials()
    {
        childMaterials.Clear();
        foreach (Transform child in transform)
        {
            var meshRenderer = child.GetComponent<MeshRenderer>();
            if (meshRenderer)
            {
                foreach (var material in meshRenderer.materials)
                    childMaterials.Add(material);
            }
        }
    }

    public void ApplyDisplay(bool enable)
    {
        UpdateChildMaterials();
        if (displayType == DisplayType.RADIO)
            ApplyDisplayRadio(enable);
    }

    private void ApplyDisplayRadio(bool enable)
    {
        foreach (var m in childMaterials)
        {
            Color colorToSet = enable ? Color.white : Color.black;
            m.color = colorToSet;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if ((IsEditorGroundType() && !other.CompareTag("ground"))
            || (IsEditorVerticalType() && !other.CompareTag("vertical")))
        {
            if (taken && halo != null)
                halo.enabled = true;
            canBePlaced = false;
        }
    }

    public void Delete()
    {
        DestroyImmediate(gameObject);
    }

    public void EnableBoxColliders(bool enable)
    {
        foreach (BoxCollider b in GetComponents<BoxCollider>())
            b.enabled = enable;
        foreach (BoxCollider b in GetComponentsInChildren<BoxCollider>())
            b.enabled = enable;
        foreach (MeshCollider m in GetComponents<MeshCollider>())
            m.enabled = enable;
        foreach (MeshCollider m in GetComponentsInChildren<MeshCollider>())
            m.enabled = enable;
    }

    private void SetIsTrigger(bool enable)
    {
        foreach (BoxCollider b in GetComponents<BoxCollider>())
            b.isTrigger = enable;
        foreach (BoxCollider b in GetComponentsInChildren<BoxCollider>())
            b.isTrigger = b.name == "InteractionBox" ? b.isTrigger : enable; // Not changing isTrigger for interactionbox
        foreach (MeshCollider b in GetComponents<MeshCollider>())
            b.isTrigger = enable;
        foreach (MeshCollider b in GetComponentsInChildren<MeshCollider>())
        {
            if (!b.convex)
                print(b.name + " not convex MeshCollider");

            b.isTrigger = enable;
        }
    }

    public void Throw()
    {
        thrown = true;
        Drop(new Vector3(200, -200, 200));
        DisplayRenderer(false);
    }

    private bool HasInnerSlots()
    {
        return GetComponentInChildren<SlotController>() != null;
    }

    public bool IsEditorType()
    {
        return collectableType == CollectableType.NPC_EDITOR 
            || collectableType == CollectableType.FURNITURE_EDITOR
            || collectableType == CollectableType.VERTICAL_FURNITURE_EDITOR
            || collectableType == CollectableType.CEILING_FURNITURE_EDITOR;
    }

    public bool IsEditorGroundType()
    {
        return collectableType == CollectableType.NPC_EDITOR 
            || collectableType == CollectableType.FURNITURE_EDITOR;
    }

    public bool IsEditorCeilingType()
    {
        return collectableType == CollectableType.CEILING_FURNITURE_EDITOR;
    }

    public bool IsEditorVerticalType()
    {
        return collectableType == CollectableType.VERTICAL_FURNITURE_EDITOR;
    }

    public bool IsEditorFurnitureType()
    {
        return collectableType == CollectableType.FURNITURE_EDITOR 
            || collectableType == CollectableType.VERTICAL_FURNITURE_EDITOR
            || collectableType == CollectableType.CEILING_FURNITURE_EDITOR;
    }

    private bool IsEditorRotatableType()
    {
        return collectableType == CollectableType.FURNITURE_EDITOR 
            || collectableType == CollectableType.NPC_EDITOR
            || collectableType == CollectableType.CEILING_FURNITURE_EDITOR;
    }

    public bool IsEditorTypeAndCorrectTag(string tag)
    {
        return (IsEditorGroundType() && tag == "ground") 
            || (IsEditorVerticalType() && tag == "vertical")
            || (IsEditorCeilingType() && tag == "ceiling");
    }

    public bool IsCollectableType()
    {
        return collectableType == CollectableType.DISC 
            || collectableType == CollectableType.REGULAR 
            || collectableType == CollectableType.VERTICAL;
    }

    public bool IsNPC()
    {
        return collectableType == CollectableType.NPC_EDITOR;
    }
}
