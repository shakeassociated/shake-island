﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveLockerOpenButtonController : InteractionButtonController
{
    InteractiveLockerController interactiveLockerController;
    TextMesh textMesh;

    private void Awake()
    {
        interactiveLockerController = transform.parent.parent.GetComponent<InteractiveLockerController>();
        textMesh = GetComponentInChildren<TextMesh>();
    }

    public override void Start()
    {
        base.Start();
    }

    public override void DoAction()
    {
        interactiveLockerController.ChangeState();
    }

    public void SetText(string newText)
    {
        textMesh.text = newText;
    }
}
