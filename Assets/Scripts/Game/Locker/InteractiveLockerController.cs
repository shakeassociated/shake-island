﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveLockerController : MonoBehaviour {
    Animator _animator;
    bool opening, closing;
    enum DoorState { OPEN, CLOSED };
    DoorState doorState;
    BoxCollider doorCollider;

    InteractiveLockerOpenButtonController interactiveLockerOpenButtonController;

    void Start ()
    {
        _animator = GetComponent<Animator>();
        opening = false;
        closing = false;
        doorState = DoorState.CLOSED;
        doorCollider = transform.Find("DoorCollider").GetComponent<BoxCollider>();

        interactiveLockerOpenButtonController = GetComponentInChildren<InteractiveLockerOpenButtonController>();
	}
	
	void Update ()
    {
        if (opening && _animator.GetCurrentAnimatorStateInfo(0).IsName("Open"))
        {
            opening = false;

            doorState = DoorState.OPEN;
            doorCollider.enabled = false;
            interactiveLockerOpenButtonController.SetText("Fermer");
        }

        if (closing && !_animator.GetCurrentAnimatorStateInfo(0).IsName("Open"))
        {
            closing = false;

            doorState = DoorState.CLOSED;
            doorCollider.enabled = true;
            interactiveLockerOpenButtonController.SetText("Ouvrir");
        }
    }

    public void ChangeState()
    {
        if (doorState == DoorState.CLOSED)
        {
            _animator.SetBool("open", true);
            opening = true;
        }
        else
        {
            _animator.SetBool("open", false);
            closing = true;
        }
    }

    public void Open()
    {
        if (doorState == DoorState.CLOSED)
        {
            _animator.SetBool("open", true);
            opening = true;
        }
    }

    public void Shake()
    {
        Open();
    }
}
