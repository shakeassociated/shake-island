﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

public class InternetCheck : MonoBehaviour {

    public static bool IsInternetOn()
    {
        try
        {
            using (var client = new WebClient())
            {
                using (var stream = client.OpenRead("http://www.google.com"))
                {
                    return true;
                }
            }
        }
        catch
        {
            return false;
        }
    }
}
