﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AuthController : MonoBehaviour {
    GameObject choosePanel, connexionPanel, inscriptionPanel;
    public UsersController usersController;
    public MapsController mapsController;
    public enum AuthState { CHOOSE, CONNEXION, INSCRIPTION };

    private void Awake()
    {
        choosePanel = transform.Find("ChoosePanel").gameObject;
        connexionPanel = transform.Find("ConnexionPanel").gameObject;
        inscriptionPanel = transform.Find("InscriptionPanel").gameObject;
    }

    public bool IsConnected()
    {
        return PlayerPrefs.HasKey("jwt");
    }

    public void Open(string str)
    {
        var authState = strToState(str);

        gameObject.SetActive(true);
        choosePanel.SetActive(authState == AuthState.CHOOSE);
        connexionPanel.SetActive(authState == AuthState.CONNEXION);
        inscriptionPanel.SetActive(authState == AuthState.INSCRIPTION);
    }

    private AuthState strToState(string str)
    {
        AuthState res = AuthState.CHOOSE;

        switch (str)
        {
            case "LOGIN":
                res = AuthState.CONNEXION;
                break;
            case "INSCRIPTION":
                res = AuthState.INSCRIPTION;
                break;
        }

        return res;
    }

    public void Close()
    {
        choosePanel.SetActive(false);
        connexionPanel.SetActive(false);
        inscriptionPanel.SetActive(false);
        gameObject.SetActive(false);
    }

    public void ClearUserData()
    {
        PlayerPrefs.DeleteAll();
    }
}
