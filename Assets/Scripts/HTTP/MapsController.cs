﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using System.IO;

[System.Serializable]
public class Map
{
    public int id, version, user_id;
    public string name, json;
    public User user;
    public MapInfo mapInfo;

    public Map(int id, string name, string json, int version, int user_id, User user)
    {
        this.id = id;
        this.name = name;
        this.json = json;
        this.version = version;
        this.user_id = user_id;
        this.user = user;

        this.mapInfo = JsonUtility.FromJson<MapInfo>(this.json);
    }

    public void Print()
    {
        Debug.Log("Map " + id + " : " + name + " by id n°" + user_id);
    }

    public string Serialize()
    {
        return JsonUtility.ToJson(this);
    }
}

public class MapsController : MonoBehaviour
{
    public IEnumerator PublishMap(MapInfo map, System.Action<bool, string> callback)
    {
        string url = HTTPProperties.base_url + "maps";

        WWWForm postData = new WWWForm();
        
        postData.AddField("token", PlayerPrefs.GetString("jwt"));
        postData.AddField("name", map.mapName);
        postData.AddField("json", map.Serialize());

        WWW www = new WWW(url, postData);
        yield return www;
        
        callback(www.error == null, www.text);
    }

    public IEnumerator GetMapsVersions(System.Action<bool, string> callback)
    {
        string url = HTTPProperties.base_url + "maps/versions";
        WWW www = new WWW(url);
        yield return www;

        if (www.error == null)
        {
            callback(true, www.text);
        }
        else
        {
            callback(false, www.error);
        }
    }

    

    public IEnumerator GetAllMaps(System.Action<bool, List<Map>> callback)
    {        
        string url = HTTPProperties.base_url + "maps";
        WWW www = new WWW(url);
        yield return www;

        List<Map> res = new List<Map>();

        if (www.error == null)
        {
            var json = JSON.Parse(www.text);
            for (int i = 0; i < json.Count; i++)
            {
                var map = json[i];
                res.Add(new Map(map["id"], map["name"], map["json"], map["version"], map["user_id"], new User(map["user"])));
            }

            callback(true, res);
        }
        else
            callback(false, null);
    }

    public IEnumerator TestCoco(System.Action<bool> callback)
    {
        string url = HTTPProperties.base_url + "maps/versions";
        WWW www = new WWW(url);
        yield return www;

        callback(false);
        print("mabite ?");
        callback(true);
    }
}
