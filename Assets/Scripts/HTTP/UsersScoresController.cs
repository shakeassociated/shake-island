﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class UsersScoresController : MonoBehaviour
{
    public void InvokeGetUserScores(int userId)
    {
        StartCoroutine(GetUserScores(userId));
    }

    public IEnumerator GetUserScores(int userId)
    {
        if (!PlayerPrefs.HasKey("jwt"))
            throw new System.Exception("User must connect first.");

        string url = HTTPProperties.base_url + "users/" + userId + "/scores";
        
        var headers = new Dictionary<string, string>();
        headers.Add("Authorization", "Bearer " + PlayerPrefs.GetString("jwt"));

        WWW www = new WWW(url, null, headers);
        yield return www;

        if (www.error == null)
        {
            Debug.Log("res = " + www.text);
        }
        else
        {
            Debug.Log(www.error);
        }
    }
}
