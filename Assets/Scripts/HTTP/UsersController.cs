﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

[System.Serializable]
public class User
{
    public int id;
    public string pseudo, email;

    public User(JSONNode user)
    {
        this.id = user["id"];
        this.pseudo = user["pseudo"];
        this.email = user["email"];
    }

    public User(int id, string pseudo, string email)
    {
        this.id = id;
        this.pseudo = pseudo;
        this.email = email;
    }

    public void Print()
    {
        Debug.Log("User " + id + " : " + pseudo + " | " + email);
    }
}

[System.Serializable]
public class LoginResponse
{
    public string jwt;
    public User user;
    

    public LoginResponse(string jwt, User user)
    {
        this.jwt = jwt;
        this.user = user;
    }
}

public class UsersController : MonoBehaviour
{
    // Starts the coroutine
    // Retrieves the result in the lambda expression
    public void InvokeGetAllUsers()
    {
        StartCoroutine(
            GetAllUsers((resBool, users) =>
            {
                if (resBool)
                {
                    foreach (User user in users)
                    {
                        user.Print();   
                    }
                }
                else
                {
                    Debug.Log("failed fetching");
                }
            }
        ));
    }

    // This waits for the end of the request to return
    // The result is in the callback, second argument
    public IEnumerator GetAllUsers(System.Action<bool, List<User>> callback)
    {
        string url = HTTPProperties.base_url + "users";
        WWW www = new WWW(url);
        yield return www;


        List<User> res = new List<User>();
        
        if (www.error == null)
        {
            var json = JSON.Parse(www.text);
            for (int i = 0; i < json.Count; i++)
            {
                var user = json[i];
                res.Add(new User(user));
            }

            callback(true, res);
        }
        else
        {

            callback(false, null);
        }
    }

    public IEnumerator Login(string pseudo, string password, System.Action<bool, LoginResponse> callback)
    {
        string url = HTTPProperties.base_url + "login";

        WWWForm postData = new WWWForm();
        postData.AddField("pseudo", pseudo);
        postData.AddField("password", password);

        WWW www = new WWW(url, postData);
        yield return www;

        if (www.error == null)
        {
            var json = JSON.Parse(www.text);

            LoginResponse res = new LoginResponse(json["token"], new User(json["user"]));
            PlayerPrefs.SetString("jwt", json["token"]);

            callback(true, res);
        }
        else
        {
            callback(false, new LoginResponse("Not connected", null));
        }
    }

    /// <summary>
    /// Registers the user and returns the token, if correctly created
    /// </summary>
    /// <param name="pseudo"></param>
    /// <param name="password"></param>
    /// <param name="email"></param>
    /// <param name="callback"></param>
    /// <returns></returns>
    public IEnumerator Register(string pseudo, string password, string email, System.Action<bool, LoginResponse> callback)
    {
        string url = HTTPProperties.base_url + "users";

        WWWForm postData = new WWWForm();
        postData.AddField("pseudo", pseudo);
        postData.AddField("password", password);
        postData.AddField("email", email);

        WWW www = new WWW(url, postData);
        yield return www;

        if (www.error == null)
        {
            var json = JSON.Parse(www.text);

            LoginResponse res = new LoginResponse(json["token"], new User(json["user"]));
            PlayerPrefs.SetString("jwt", json["token"]);

            callback(true, res);
        }
        else
        {
            callback(false, new LoginResponse("Failed to sign in", null));
        }
    }
}
