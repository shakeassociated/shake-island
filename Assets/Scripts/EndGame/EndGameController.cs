﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndGameController : MonoBehaviour
{
    public Text percentage, feedback, totalTime, nbObjects;

    void Start()
    {
        var score = LevelProperties.score;

        if (score != null)
        {
            percentage.text = ((int)score.notePercentage).ToString() + "%";
            feedback.text = score.ComputeFeedBack();
            totalTime.text = score.TotalTimeToString();
            
            var nbCorrect = score.nbCorrectPerfs + score.nbAuditiveCorrect + score.nbRegCorrect + score.nbVertCorrect;
            nbObjects.text = nbCorrect.ToString() + " / " + score.nbCollectables.ToString();
        }
    }
}
