﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMenuController : MonoBehaviour
{
    public MenuController menuController;
    Vector3 originPos;
    Quaternion originRot;

    private void Start()
    {
        originPos = transform.position;
        originRot = transform.rotation;
    }

    public void AnimationIsOver()
    {
        menuController.SetToCorrectState();
        transform.position = originPos;
        transform.rotation = originRot;
    }
}
