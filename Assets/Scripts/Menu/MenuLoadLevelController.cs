﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuLoadLevelController : MonoBehaviour
{
    public GameObject panelLevelPrefab;
    MapInfo currMapInfo;
    GameObject currMapPanel;

    private void Start()
    {
        Refresh();
    }

    private void Clear()
    {
        foreach (Transform child in transform)
            Destroy(child.gameObject);

        currMapInfo = null;
        currMapPanel = null;
    }

    public void Refresh()
    {
        Clear();

        var dir = new DirectoryInfo(Application.persistentDataPath + "/Maps/Local/");
        var files = dir.GetFiles("*.txt", SearchOption.AllDirectories);

        int n = 0;
        foreach (var file in files)
        {
            var jsonLoaded = File.ReadAllText(file.FullName);
            MapInfo mapInfo = JsonUtility.FromJson<MapInfo>(jsonLoaded);
            GenerateObjectInList(mapInfo, n);

            n++;
        }
    }

    private GameObject GenerateObjectInList(MapInfo mapInfo, int n)
    {
        var o = Instantiate(panelLevelPrefab);

        // Script on Content
        o.transform.SetParent(transform);

        // Set correct position and scale
        var rectTransform = o.GetComponent<RectTransform>();
        rectTransform.anchoredPosition = panelLevelPrefab.GetComponent<RectTransform>().anchoredPosition;

        var newLocalPosition = new Vector3(rectTransform.localPosition.x, rectTransform.localPosition.y - n * 20, 0);
        rectTransform.localPosition = newLocalPosition;
        rectTransform.localScale = new Vector3(1, 1, 1);

        var text = o.transform.Find("Text").GetComponent<Text>();
        text.text = mapInfo.mapName;
        text.color = new Color(0, 0, 0);

        var trigger = o.GetComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerClick;
        entry.callback.AddListener((eventData) => { ChangeCurrMap(o, mapInfo); });
        trigger.triggers.Add(entry);

        return o;
    }

    private void ChangeCurrMap(GameObject panel, MapInfo mapInfo)
    {
        if (currMapPanel != null)
            currMapPanel.GetComponent<Image>().enabled = false;

        currMapPanel = panel;
        currMapInfo = mapInfo;
        currMapPanel.GetComponent<Image>().enabled = true;
    }

    public void Play()
    {
        if (currMapInfo != null)
        {
            LevelProperties.levelTypeToLoad = LevelProperties.LevelType.LOCAL;
            LevelProperties.levelToLoad = currMapInfo.mapName;
            SceneManager.LoadScene("InGame");
        }
    }

    public void Edit()
    {
        if (currMapInfo != null)
        {
            LevelEditorProperties.levelName = currMapInfo.mapName;
            LevelProperties.levelTypeToLoad = LevelProperties.LevelType.LOCAL;
            LevelProperties.levelToLoad = currMapInfo.mapName;
            SceneManager.LoadScene("LevelEditor");
        }
    }

    public void Delete()
    {
        if (currMapInfo != null)
        {
            File.Delete(Application.persistentDataPath + "/Maps/Local/" + currMapInfo.mapName + ".txt");
            Refresh();
        }
    }
}
