﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuAudioController : MonoBehaviour
{
    private Slider volumeSlider;
    private Toggle subtitlesToggle;

    private void Awake()
    {
        volumeSlider = GetComponentInChildren<Slider>();
        subtitlesToggle = GetComponentInChildren<Toggle>();
    }

    private void Start()
    {
        volumeSlider.value = LevelProperties.volume;
        subtitlesToggle.isOn = LevelProperties.subtitles;
    }

    public void AdjustVolume()
    {
        AudioListener.volume = volumeSlider.value;
        LevelProperties.volume = volumeSlider.value;
    }

    public void ChangeSubtitles()
    {
        LevelProperties.subtitles = !LevelProperties.subtitles;
    }
}
