﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuNewLevelController : MonoBehaviour {
    public InputField levelNameInput;
    public Dropdown levelTypeDropdown;

    public void Create()
    {
        LevelEditorProperties.levelName = levelNameInput.text;

        LevelProperties.levelTypeToLoad = LevelProperties.LevelType.RESOURCE;
        LevelProperties.levelToLoad = IntToArchitecture(levelTypeDropdown.value).ToString().ToLower();

        SceneManager.LoadScene("LevelEditor");
    }

    private LevelEditorProperties.LevelArchitecture IntToArchitecture(int x)
    {
        switch (x)
        {
            case 0:
                return LevelEditorProperties.LevelArchitecture.CUBE;
            case 1:
                return LevelEditorProperties.LevelArchitecture.HOUSE;
            case 2:
                return LevelEditorProperties.LevelArchitecture.HOSPITAL;
            case 3:
                return LevelEditorProperties.LevelArchitecture.GYM;
        }
        return LevelEditorProperties.LevelArchitecture.CUBE;
    }
}
