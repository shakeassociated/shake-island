﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoginScript : MonoBehaviour {
    public AuthController authController;
    public InputField loginfield, passwordfield;
    public Text error;
    public Button connect;

    private void Start()
    {
        error.text = "";
    }

    public void Login()
    {
        if (connect.IsInteractable())
        {
            connect.interactable = false;
            StartCoroutine(authController.usersController.Login(
                loginfield.text,
                passwordfield.text,
                (resBool, data) =>
                {
                    connect.interactable = true;

                    if (resBool)
                    {
                        var token = data.jwt;
                        PlayerPrefs.SetString("jwt", token);
                        PlayerPrefs.SetString("pseudo", data.user.pseudo);
                        error.text = "";
                        authController.Close();
                    }
                    else
                    {
                        error.text = "Connexion échouée";
                    }
                }));
        }
    }
}
