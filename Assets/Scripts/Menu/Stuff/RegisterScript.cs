﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RegisterScript : MonoBehaviour
{
    public AuthController authController;
    public InputField usernamefield, passwordfield, emailfield;
    public Text error;
    public Button register;
    public CheckConfirmationScript checkConfirm;
    bool isFetching;

    private void Start()
    {
        isFetching = false;
        error.text = "";
    }

    private void Update()
    {
        register.interactable = !isFetching && checkConfirm.IsValid();
    }

    public void Register()
    {
        if (register.IsInteractable())
        {
            isFetching = true;
            StartCoroutine(authController.usersController.Register(
            usernamefield.text,
            passwordfield.text,
            emailfield.text,
            (resBool, data) =>
            {
                isFetching = false;
                if (resBool)
                {
                    var token = data.jwt;
                    PlayerPrefs.SetString("jwt", token);
                    PlayerPrefs.SetString("pseudo", data.user.pseudo);
                    error.text = "";
                    authController.Close();
                }
                else
                {
                    error.text = "Inscription impossible";
                }
            }));
        }
    }
}
