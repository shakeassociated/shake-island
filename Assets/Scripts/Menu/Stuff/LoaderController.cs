﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoaderController : MonoBehaviour
{
    bool displayed;
    RawImage rawImage;

    private void Awake()
    {
        rawImage = GetComponent<RawImage>();
    }

    void Start()
    {
        displayed = false;
        rawImage.enabled = false;
    }

    public void Activate()
    {
        displayed = true;
        rawImage.enabled = true;
    }
    
    public void Desactivate()
    {
        displayed = false;
        rawImage.enabled = false;
    }

    void Update()
    {
        if (displayed)
            transform.Rotate(new Vector3(0, 0, -100 * Time.deltaTime));
    }
}
