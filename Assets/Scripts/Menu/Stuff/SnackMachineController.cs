﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class SnackMachineController : MonoBehaviour
{
    public Text selectText;
    private bool canSelect;

    private void Start()
    {
        canSelect = false;
    }

    private void Update()
    {
        selectText.enabled = canSelect;

        if (canSelect && Input.GetMouseButton(0))
        {
            FindObjectOfType<FirstPersonController>().enabled = false;

            ObjectSelectorController objSelCol = FindObjectOfType<ObjectSelectorController>();
            objSelCol.canvasObjectSelector.SetActive(true);
            objSelCol.Init();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            canSelect = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            canSelect = false;
        }
    }
}
