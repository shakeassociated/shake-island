﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadMapController : MonoBehaviour {

    public string NameNextScene;
    MapController mapController;
    public RawImage loader;
    bool internet, alreadyLoaded, isLoading;

    private void Awake()
    {
        mapController = GetComponent<MapController>();
    }

    void Start () {
        alreadyLoaded = false;
        isLoading = false;
	}

    public void Update()
    {
        if (alreadyLoaded)
        {
            if (internet && mapController.GetRefreshingMaps())
            {
                loader.GetComponent<LoaderController>().Activate();
            }
            else
            {
                Debug.Log("Loading finished");
                SceneManager.LoadScene(NameNextScene, LoadSceneMode.Single);
            }
        }
        else
        {
            if (isLoading)
            {
                alreadyLoaded = mapController.RefreshingMaps;
            }
            else
            {
                internet = InternetCheck.IsInternetOn();
                // Vérifie que les maps sont bien les bonnes si internet il y a. Les téléchargent si besoin.
                try
                {
                    if (internet)
                    {
                        // Télécharge les maps ou les met à jours
                        //Debug.Log("Loading maps");
                        mapController.RefreshMaps();
                        isLoading = true;
                    }
                    else
                    {
                        Debug.Log("No internet connection");
                        alreadyLoaded = true;
                    }
                }
                catch (System.Exception)
                {
                    // On log l'exception avec le plus grand calme
                    // On prévient l'utilisateur que un problème est survenu pendant le téléchargement.
                }
                finally
                {
                    // On charge le prochain niveau dans tout les cas quand le truc du haut est fini

                }
            }
        }
    }
}
