﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class isWin : MonoBehaviour {
    public Text winText;

    private void Awake()
    {
        winText = GetComponent<Text>();
    }

    void Start () {
        winText.text = "Score: " + Mathf.Round(LevelProperties.score.notePercentage * 100f) / 100f + " %";
    }
}
