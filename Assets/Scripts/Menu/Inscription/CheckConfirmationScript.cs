﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckConfirmationScript : MonoBehaviour {
    private InputField inputField;
    public Image checksign;
    public InputField passwordField;
    bool isValid;

    public bool IsValid()
    {
        return isValid;
    }

    private void Awake()
    {
        inputField = GetComponent<InputField>();
    }

    private void Update()
    {
        isValid = passwordField.text == inputField.text;
        checksign.enabled = !isValid;
    }
}
