﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ValidateAllFields : MonoBehaviour {

    public InputField usernameField;
    public InputField passwordField;
    public InputField confirmField;
    public InputField emailField;
    public Button registerButton;


    bool AllValids()
    {
        CheckUsernameScript checkUsernameScript = usernameField.GetComponent<CheckUsernameScript>();
        CheckPasswordScript checkPasswordScript = passwordField.GetComponent<CheckPasswordScript>();
        CheckConfirmationScript checkConfirmationScript = confirmField.GetComponent<CheckConfirmationScript>();
        CheckEmailScript checkEmailScript = emailField.GetComponent<CheckEmailScript>();

        return checkUsernameScript.IsValid() && checkPasswordScript.IsValid() && checkConfirmationScript.IsValid() && checkEmailScript.IsValid();
    }
	// Update is called once per frame
	void Update () {
		if (AllValids() && InternetCheck.IsInternetOn())
        {
            registerButton.interactable = true;
        }
        else
        {
            registerButton.interactable = false;
        }
	}
}
