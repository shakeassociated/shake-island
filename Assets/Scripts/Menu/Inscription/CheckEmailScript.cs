﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class CheckEmailScript : MonoBehaviour {

    private InputField inputField;
    public Image checksign;
    public Sprite Cross;
    public Sprite Check;
    bool isValid;

    public bool IsValid()
    {
        return isValid;
    }

	// Use this for initialization
    void Start () {
        inputField = GetComponent<InputField>();
        inputField.onValueChanged.AddListener(delegate { CheckEmail(); });
    }

    void CheckEmail()
    {
        Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
        Match match = regex.Match(inputField.text);
        if (match.Success)
        {
            checksign.sprite = Check;
            isValid = true;
        }
        else
        {
            checksign.sprite = Cross;
            isValid = false;
        }
    }

}
