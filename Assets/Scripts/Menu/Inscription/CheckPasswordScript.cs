﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class CheckPasswordScript : MonoBehaviour {

    private InputField inputField;
    public Image checksign;
    public Sprite cross;
    public Sprite check;
    bool isValid;

    public bool IsValid()
    {
        return isValid;
    }

    public void Start()
    {
        inputField = GetComponent<InputField>();
        inputField.onValueChanged.AddListener(delegate { CheckPassword(); });
    }

    public void CheckPassword()
    {
        Regex regex = new Regex(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,15}$");
        Match match = regex.Match(inputField.text);
        if (match.Success)
        {
            checksign.sprite = check;
            isValid = true;
        }
        else
        {
            checksign.sprite = cross;
            isValid = false;
        }
    }
}
