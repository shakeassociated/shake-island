﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class CheckUsernameScript : MonoBehaviour {

    public UsersController usersController;
    private InputField inputField;
    public Image checksign;
    bool checkUserExists;
    public Sprite cross;
    public Sprite check;
    bool isValid;

    public bool IsValid()
    {
        return isValid;
    }

    // Use this for initialization
    public void Start()
    {

        inputField = GetComponent<InputField>();
        checkUserExists = true;
        //inputField.onValueChanged.AddListener(delegate { CheckUsername(); });
        inputField.onEndEdit.AddListener(delegate { CheckUsername(); });
    }

    public bool CheckUserExists(string username)
    {
        if (InternetCheck.IsInternetOn())
        {
            StartCoroutine(usersController.GetAllUsers((resBool, res) =>
            {
                if (resBool)
                {
                    foreach (User user in res)
                    {
                        if (username.Equals(user.pseudo))
                        {
                            checkUserExists = true;
                            return;
                        }
                    }
                    checkUserExists = false;
                }
                else
                {

                }
            }));
        }
        return false;
    }

    public void CheckUsername()
    {
        if (inputField.text.Length != 0)
            CheckUserExists(inputField.text);
    }

    public void Update()
    {
        Debug.Log(checkUserExists);
        if (!checkUserExists)
        {
            checksign.sprite = check;
            isValid = true;
        }
        else
        {
            checksign.sprite = cross;
            isValid = false;
        }
    }
}
