﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour {
    public GameObject mainMenuPanel, playerLevelSelector, backgroundCamera, title;
    public Animator camera_ac;
    bool isGoingToLevelSelector;
    public enum positionState { MAIN_MENU, LEVEL_SELECTOR }
    public positionState currPositionState;

    private void Start()
    {
        isGoingToLevelSelector = false;
        if (MenuProperties.isPlayer)
            SetLevelSelector();
        else
            currPositionState = positionState.MAIN_MENU;
    }

    public void StartLevelSelector()
    {
        mainMenuPanel.SetActive(false);
        title.SetActive(false);

        if (currPositionState == positionState.MAIN_MENU)
        {
            camera_ac.SetBool("LevelSelector", true);
            isGoingToLevelSelector = true;
        }
        else
        {
            SetLevelSelector();
        }
    }

    public void SetToCorrectState()
    {
        if (isGoingToLevelSelector)
            SetLevelSelector();
        else
            SetMenu();
    }

    public void SetLevelSelector()
    {
        currPositionState = positionState.LEVEL_SELECTOR;

        mainMenuPanel.SetActive(false);
        title.SetActive(false);
        backgroundCamera.SetActive(false);

        playerLevelSelector.SetActive(true);
        isGoingToLevelSelector = false;

        MenuProperties.isPlayer = true;
    }

    public void SetMenu()
    {
        if (!isGoingToLevelSelector)
        {
            playerLevelSelector.SetActive(false);

            mainMenuPanel.SetActive(true);
            title.SetActive(true);
            backgroundCamera.SetActive(true);
            isGoingToLevelSelector = true;

            MenuProperties.isPlayer = false;
            LevelProperties.score = null;
        }
    }
}
