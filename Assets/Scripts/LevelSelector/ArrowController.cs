﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowController : MonoBehaviour
{
    public enum ArrowDirection { FORWARD, RIGHT, BACKWARD, LEFT };
    public ArrowDirection direction;
    Material matToChange;

    private void Awake()
    {
        matToChange = transform.FindDeepChild("Plane").GetComponent<SkinnedMeshRenderer>().material;
    }

    public void SetFocused(bool focus)
    {
        matToChange.color = new Color(matToChange.color.r, matToChange.color.g, matToChange.color.b, focus ? 1 : 0.67f);
    }
}
