﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpotController : MonoBehaviour {
    public int id;
    public bool onlyForwardLook;
    public List<GameObject> listHouses;
    public List<GameObject> arrows;

    public void EnableArrows(bool val)
    {
        foreach (var a in arrows)
            a.SetActive(val);
    }
}
