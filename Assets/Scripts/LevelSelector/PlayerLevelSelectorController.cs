﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerLevelSelectorController : MonoBehaviour
{
    bool isMovingForward, isTurning;
    int lastSpotId, currentSpotId, nbSpots;
    public SpotController currSpotController;
    HouseController currHouseController;
    public float speed, speedRotation;
    public Text canvasText;
    public GameObject startButton, canvas, secondCanvas;
    int idLevel;
    private GraphGenerator graphgenerator;
    new Camera camera;
    ArrowController lastArrowFocused;
    MenuController menuController;
    public GameObject containerRecap;
    bool isInRecap, isInRecapCommunity;
    public Text timePastRecap, scoreRecap, feedBackRecap, nbObjectsRecap;
    public GameObject communityPanel;
    Map currMap;
    public GameObject recapButton;

    enum OrientationState
    {
        LEFT,
        UP,
        RIGHT
    };
    OrientationState orientation;

    public GameObject communityContainer;

    private void Awake()
    {
        camera = transform.Find("Camera").GetComponent<Camera>();
        menuController = FindObjectOfType<MenuController>();
    }

    private void Start()
    {
        lastSpotId = 0;
        currentSpotId = 0;
        isMovingForward = false;
        isTurning = false;
        speed = 12f;
        speedRotation = 35f;
        orientation = OrientationState.UP;
        nbSpots = 3;
        currHouseController = null;
        graphgenerator = FindObjectOfType<GraphGenerator>();
        lastArrowFocused = null;
        if (LevelProperties.score != null)
            fillRecap();
        recapButton.SetActive(false);
    }

    private void fillRecap()
    {
        containerRecap.SetActive(true);
        timePastRecap.text = TimeUtils.PrettyPrintTime(LevelProperties.score.timeSpentResolving);
        scoreRecap.text = LevelProperties.score.notePercentage.ToString("F2") + "%";
        feedBackRecap.text = LevelProperties.score.feedback;
        float nbObjectCorrects = LevelProperties.score.nbAuditiveCorrect + LevelProperties.score.nbVertCorrect + LevelProperties.score.nbRegCorrect;
        nbObjectsRecap.text = (int)nbObjectCorrects + " / " + LevelProperties.score.nbCollectables;
        isInRecap = true;
    }

    void Update()
    {

        if (!isMovingForward && !isInRecap)
        {
            if (!isTurning && orientation == OrientationState.UP && !currSpotController.onlyForwardLook && Input.GetKeyDown(KeyCode.Escape))
                menuController.SetMenu();

            var vertical = 0f;
            if (TeamUtility.IO.InputManager.Instance)
                vertical = TeamUtility.IO.InputManager.GetAxisConfiguration("KeyboardAndMouse", "Vertical").GetAxis(); // - : Down; + : Up
            else
                vertical = Input.GetAxis("Vertical");

            if (!isTurning && vertical != 0 && CanMove(vertical > 0))
                StartCoroutine(Move(vertical > 0));

            if (!isTurning && currSpotController != null && !currSpotController.onlyForwardLook)
            {
                var horizontal = 0f;
                if (TeamUtility.IO.InputManager.Instance)
                    horizontal = TeamUtility.IO.InputManager.GetAxisConfiguration("KeyboardAndMouse", "Horizontal").GetAxis();// - : Left; + : Right
                else
                    horizontal = Input.GetAxis("Horizontal");

                // si déplacement vertical pendant rotation, déplacement éffectué après quand même
                if (horizontal != 0)
                    StartCoroutine(Look(horizontal > 0));
            }

            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            bool rayCast = Physics.Raycast(ray, out hit, 20);
            InterpreteRaycast(rayCast, hit);
        }
    }

    // idHouse = 0 left | -1 middle | 1 right 
    void SetLevelInfo(int idHouse)
    {
        if (idHouse == -1)
        {
            currSpotController.EnableArrows(true);
            currHouseController = null;
        }
        else
        {

            HouseController hc = currSpotController.listHouses[idHouse].GetComponent<HouseController>();
            if (hc)
            {
                var filePath = "";
                if (currSpotController.id == 2)
                {
                    currMap = communityPanel.GetComponent<CommunityPanelController>().GetCurrMap();
                    if (currMap != null)
                    {
                        filePath = Application.persistentDataPath + "/Scores/Downloaded/" + currMap.user_id + "/" + currMap.name + ".txt";
                        Debug.Log(filePath);
                    }
                    else
                        filePath = "";
                }
                else
                {
                    filePath = Application.persistentDataPath + "/Scores/Native/" + hc.levelToLoad + ".txt";
                }
                hc.performances.Clear();
                if (File.Exists(filePath))
                {
                    var json = File.ReadAllText(filePath);
                    Scores scores = JsonUtility.FromJson<Scores>(json);
                    hc.UpdateScoresCommunity(scores);
                    canvasText.text = currSpotController.id == 2 ? currMap.name : hc.pretty_name;
                }
                else
                {
                    hc.pretty_best_time_string = "";
                    hc.last_feedback = "";
                }

                canvas.SetActive(true);
                secondCanvas.SetActive(true);
                graphgenerator.destroyPreviewMini();
                if (hc.previewMini != null)
                    graphgenerator.displayPreviewMini(hc.previewMini, hc.offsetY, hc.scalePreviewMini);
                idLevel = hc.id;
                currHouseController = hc;
                graphgenerator.destroyArray();
                graphgenerator.deleteText();
                if (hc.pretty_name != "Tutoriel")
                {
                    graphgenerator.displayArray(hc.performances);
                    graphgenerator.displayText(hc.pretty_best_time_string, hc.last_feedback);
                }
            }
        }
    }

    void ActivateCommunityPanel(bool val)
    {
        communityContainer.gameObject.SetActive(val);
    }

    IEnumerator Move(bool forward)
    {
        isMovingForward = true;
        if (orientation != OrientationState.UP)
            StartCoroutine(Look(orientation == OrientationState.LEFT));

        while (isTurning)
            yield return null;

        currSpotController.EnableArrows(false);
        // Makes HUD disapear if walking back from the end of the line
        if (!forward)
        {
            canvas.SetActive(false);
            secondCanvas.SetActive(false);
            ActivateCommunityPanel(false);
        }

        while (currentSpotId == lastSpotId)
        {
            transform.position = new Vector3(
                transform.position.x,
                transform.position.y,
                transform.position.z + (forward ? 1 : -1) * speed * Time.deltaTime);

            yield return null;
        }

        lastSpotId = currentSpotId;
        currSpotController.EnableArrows(true);

        isMovingForward = false;

        if (currSpotController && currSpotController.onlyForwardLook)
        {
            canvas.SetActive(false);
            ActivateCommunityPanel(true);
        }
    }

    IEnumerator Look(bool right)
    {
        isTurning = true;

        var angleBefore = transform.rotation;
        var currAngle = angleBefore;
        var canTurn = CanChangeOrientation(right);
        currSpotController.EnableArrows(orientation == OrientationState.LEFT && right || orientation == OrientationState.RIGHT && !right);


        Quaternion beginRotation = transform.rotation;
        var yBefore = transform.rotation.eulerAngles.y;

        while (canTurn && (int)Quaternion.Angle(angleBefore, currAngle) < 38)
        {
            currAngle = transform.rotation;
            canvas.SetActive(false);
            secondCanvas.SetActive(false);
            transform.Rotate(new Vector3(
                0,
                (right ? 1 : -1) * speedRotation * Time.deltaTime,
                0));

            yield return null;
        }

        Vector3 v = transform.rotation.eulerAngles;
        transform.rotation = Quaternion.Euler(v.x, yBefore + (canTurn ? (right ? 40 : -40) : 0), v.z);

        ChangeOrientation(right);
        isTurning = false;
    }

    private bool CanMove(bool forward)
    {
        return (!forward && currentSpotId > 0) || (forward && currentSpotId < nbSpots - 1);
    }

    private bool CanChangeOrientation(bool right)
    {
        return (orientation == OrientationState.RIGHT && !right)
            || (orientation == OrientationState.LEFT && right)
            || (orientation == OrientationState.UP);
    }

    private int ChangeOrientation(bool right)
    {
        int idHouse = -1;
        switch (orientation)
        {
            case OrientationState.UP:
                orientation = right ? OrientationState.RIGHT : OrientationState.LEFT;
                idHouse = right ? 1 : 0;
                break;
            case OrientationState.LEFT:
                orientation = right ? OrientationState.UP : OrientationState.LEFT;
                idHouse = right ? -1 : 0;
                break;
            case OrientationState.RIGHT:
                orientation = right ? OrientationState.RIGHT : OrientationState.UP;
                idHouse = right ? 1 : -1;
                break;
            default:
                break;
        }

        SetLevelInfo(idHouse);

        return idHouse;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "spot")
        {
            SpotController sc = other.GetComponent<SpotController>();
            currentSpotId = sc.id;
            currSpotController = sc;
            currSpotController.EnableArrows(true);
        }
    }

    public void switchLevel()
    {
        if (currHouseController)
        {
            if (currSpotController.id == 2)
            {
                communityPanel.GetComponent<CommunityPanelController>().Play();
            }
            else
            {
                Debug.Log("Start_Game_Scene");
                LevelProperties.levelToLoad = currHouseController.levelToLoad;
                LevelProperties.levelTypeToLoad = LevelProperties.LevelType.RESOURCE;
                SceneManager.LoadScene("InGame");
            }
        }
        else
            canvasText.text = "Niveau non chargé :(";
    }

    private void InterpreteRaycast(bool rayCast, RaycastHit hit)
    {
        if (rayCast && hit.collider.CompareTag("arrow"))
        {
            var arrowCtrl = hit.collider.transform.parent.parent.parent.GetComponent<ArrowController>();

            if (arrowCtrl != lastArrowFocused)
            {
                DisableLastArrow();
                lastArrowFocused = arrowCtrl;
            }
            else
                arrowCtrl.SetFocused(true);

            if (Input.GetMouseButton(0))
            {
                var direction = arrowCtrl.direction;
                if (direction == ArrowController.ArrowDirection.BACKWARD || direction == ArrowController.ArrowDirection.FORWARD)
                    StartCoroutine(Move(direction == ArrowController.ArrowDirection.FORWARD));
                else
                    StartCoroutine(Look(direction == ArrowController.ArrowDirection.RIGHT));
            }
        }
        else
            DisableLastArrow();
    }

    private void DisableLastArrow()
    {
        if (lastArrowFocused != null)
            lastArrowFocused.SetFocused(false);
    }

    public void CancelOrientation()
    {
        Debug.Log("CancelOrientation");
        if (!currSpotController.onlyForwardLook)
        {
            Debug.Log(orientation);
            switch (orientation)
            {
                case OrientationState.LEFT:
                    StartCoroutine(Look(true));
                    break;
                case OrientationState.RIGHT:
                    StartCoroutine(Look(false));
                    break;
                default:
                    break;
            }
        }
        else
        {
            Debug.Log("LAUNCH RECAP");
            Recap();
        }
    }

    public void LeaveLastSpot()
    {
        StartCoroutine(Move(false));
    }

    public void CancelRecap()
    {
        containerRecap.SetActive(false);
        isInRecap = false;
        isInRecapCommunity = false;
    }
    public void Retry()
    {
        SceneManager.LoadScene("InGame");
    }

    public void Recap()
    {
        if (canvas.activeSelf)
        {
            canvas.SetActive(false);
            secondCanvas.SetActive(false);
            ActivateCommunityPanel(true);
            return;
        }
        if (communityContainer.activeSelf)
        {
            SetLevelInfo(0);
            ActivateCommunityPanel(false);
            return;
        }
    }
}
