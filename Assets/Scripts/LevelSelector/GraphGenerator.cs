﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GraphGenerator : MonoBehaviour
{
    public List<float> listValues = new List<float>();
    List<GameObject> listPoints;
    List<GameObject> listLines; 
    public Canvas canvas;
    float width = 600;
    float length = 420;
    float posStartX = 210;
    float posStartY = 200;
	public GameObject containerPreviewMini;
    public GameObject containerGraph;
    public GameObject containerLine;
    public GameObject Lines, linePrefab;
    public GameObject bestTimeText, feedBackText;
    public GameObject arrowsGraphContainer;
    public GameObject containerRecap;
    // Use this for initialization

    private void Start()
    {
        listPoints = new List<GameObject>();
        listLines = new List<GameObject>();
        if (LevelProperties.score != null)
            containerRecap.SetActive(true);

    }

    public void destroyArray()
    {
        foreach (GameObject go in listPoints)
        {
            Destroy(go);
        }
        listPoints.Clear();

        foreach (GameObject go in listLines)
        {
            Destroy(go);
        }
        listLines.Clear();
        arrowsGraphContainer.SetActive(false);
    }

    public void displayArray(List<float> list)
    {
        if (list != null & list.Count > 0)
        {
            foreach (GameObject go in listPoints)
            {
                Destroy(go);
            }
            listPoints.Clear();

            foreach (GameObject go in listLines)
            {
                Destroy(go);
            }
            listLines.Clear();

            if (list != null && list.Count > 0)
            {
                arrowsGraphContainer.SetActive(true);
                float posX = 0;
                Sprite texture = Resources.Load<Sprite>("black");
                GameObject g = new GameObject();
                foreach (float f in list)
                {
                    GameObject go = new GameObject();
                    go.name = "Point";
                    Image renderer = go.AddComponent<Image>();
                    renderer.sprite = texture;
                    var rectTransform = go.GetComponent<RectTransform>();
                    rectTransform.position = new Vector3(
                        containerGraph.transform.position.x - 320 + posX,
                        containerGraph.transform.position.y - 230 + ((length / 100) * f) / 2);
                    rectTransform.sizeDelta = new Vector2(width / (2 * 40), (length / 60));

                    listPoints.Add(go);
                    go.transform.SetParent(containerGraph.transform);
                    posX += width / list.Count;
                }
                for (int i = 0; i < listPoints.Count - 1; i++)
                {
                    var curr = listPoints[i];
                    var next = listPoints[i + 1];
                    GameObject newLine = Instantiate(linePrefab);
                    var imageRectTransform = newLine.GetComponent<RectTransform>();
                    Vector3 differenceVector = next.transform.position - curr.transform.position;

                    imageRectTransform.sizeDelta = new Vector2(differenceVector.magnitude, 3f);
                    imageRectTransform.pivot = new Vector2(0, 0.5f);
                    imageRectTransform.position = curr.transform.position;
                    float angle = Mathf.Atan2(differenceVector.y, differenceVector.x) * Mathf.Rad2Deg;
                    imageRectTransform.rotation = Quaternion.Euler(0, 0, angle);
                    listLines.Add(newLine);
                    imageRectTransform.transform.SetParent(Lines.transform);
                }
            }
            else
                arrowsGraphContainer.SetActive(false);
        }
    }

    public void displayText(string pretty_best_time, string last_feedback)
    {
        bestTimeText.GetComponent<Text>().text = pretty_best_time;
        feedBackText.GetComponent<Text>().text = last_feedback;
    }

    public void deleteText()
    {
        bestTimeText.GetComponent<Text>().text = "";
        feedBackText.GetComponent<Text>().text = "";
    }

    public void destroyPreviewMini()
    {
        foreach (Transform child in containerPreviewMini.transform)
        {
            GameObject.DestroyImmediate(child.gameObject);
        }
    }

    public void displayPreviewMini(GameObject obj, float offsetY, float scalePreviewMini)
	{
		foreach (Transform child in containerPreviewMini.transform) {
			GameObject.DestroyImmediate(child.gameObject);
		}

		var go = Instantiate (obj) as GameObject;
		go.transform.parent = containerPreviewMini.transform;
		go.transform.localPosition = Vector3.zero;
		go.transform.localRotation = Quaternion.identity;
		go.transform.localScale = new Vector3(scalePreviewMini, scalePreviewMini, scalePreviewMini);
		go.transform.localPosition = new Vector3 (0, offsetY, 0);
	}

    // Update is called once per frame
    void Update()
    {

    }
}
