﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CommunityPanelController : MonoBehaviour {
    GameObject leftContentGameObject, refreshing;
    public GameObject prefabObjectInList;
    CommunityPropertiesController propertiesController;
    MapsController mapsController;
    public GameObject recapButton;

    public Map currMap;
    GameObject currMapPanel;

    private void Awake()
    {
        leftContentGameObject = transform.FindDeepChild("Content").gameObject;
        refreshing = transform.FindDeepChild("Refreshing").gameObject;
        propertiesController = transform.GetComponentInChildren<CommunityPropertiesController>();
        mapsController = GetComponent<MapsController>();
    }

    private void Start()
    {
        Clear();
    }

    private void Clear()
    {
        foreach (Transform child in leftContentGameObject.transform)
            Destroy(child.gameObject);
        refreshing.SetActive(false);
        propertiesController.Clear();

        currMap= null;
        currMapPanel = null;
    }

    public Map GetCurrMap()
    {
        return currMap;
    }

    public void RefreshAllMaps()
    {
        Clear();
        refreshing.SetActive(true);
        StartCoroutine(
            mapsController.GetAllMaps((resBool, maps) =>
            {
                if (resBool)
                {
                    int n = 0;
                    foreach (Map map in maps)
                    {
                        GenerateObjectInList(map, n);
                        n++;
                    }
                }
                else
                {
                    Debug.Log("Failed fetching maps");
                }
                refreshing.SetActive(false);
            }
        ));
    }

    // Les fichiers stockés sont des Map et contiennent une MapInfo !
    public void RefreshMapsAlreadyPlayed()
    {
        Clear();

        var dir = new DirectoryInfo(Application.persistentDataPath + "/Maps/Downloaded/");
        var files = dir.GetFiles("*.txt", SearchOption.AllDirectories);

        int n = 0;
        foreach (var file in files)
        {
            var jsonLoaded = File.ReadAllText(file.FullName);
            Map map = JsonUtility.FromJson<Map>(jsonLoaded);
            GenerateObjectInList(map, n);

            n++;
        }
    }

    private GameObject GenerateObjectInList(Map map, int n)
    {
        var o = Instantiate(prefabObjectInList);
        o.transform.SetParent(leftContentGameObject.transform);

        // Set correct position and scale
        var rectTransform = o.GetComponent<RectTransform>();
        var rectTransformPrefab = prefabObjectInList.GetComponent<RectTransform>();
        rectTransform.anchoredPosition = rectTransformPrefab.anchoredPosition;
        
        var newLocalPosition = new Vector3(
            rectTransform.localPosition.x,
            rectTransform.localPosition.y - n * 26,
            0);
        rectTransform.localPosition = newLocalPosition;
        rectTransform.localScale = new Vector3(1, 1, 1);

        o.transform.Find("Text").GetComponent<Text>().text = map.name;

        var trigger = o.GetComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerClick;
        entry.callback.AddListener((eventData) => { ChangeCurrMap(o, map); });
        trigger.triggers.Add(entry);

        return o;
    }

    private void ChangeCurrMap(GameObject panel, Map map)
    {
        if (currMapPanel != null)
            currMapPanel.GetComponent<Image>().enabled = false;

        currMapPanel = panel;
        currMap = map;
        currMapPanel.GetComponent<Image>().enabled = true;
        propertiesController.SetMap(map);
        recapButton.SetActive(true);
    }

    public void Play()
    {
        print("Playing " + currMap.name + " by " + currMap.user.pseudo);
        SaveCurrMapPersistently();

        LevelProperties.levelToLoad = currMap.user_id + "/" + currMap.name;
        LevelProperties.levelTypeToLoad = LevelProperties.LevelType.DOWNLOADED;
        SceneManager.LoadScene("InGame");
    }

    public Scores GetRecap()
    {
        var filePath = Application.persistentDataPath + "/Scores/Downloaded/" + currMap.user_id + "/" + currMap.name + ".txt";
        Scores scores = null;
        if (File.Exists(filePath))
        {
            var json = File.ReadAllText(filePath);
            scores = JsonUtility.FromJson<Scores>(json);
        }
        return scores;
    }

    // Equivalent to Download
    private void SaveCurrMapPersistently()
    {
        string jsonGenerated = currMap.Serialize();
        var filePath = Application.persistentDataPath + "/Maps/Downloaded/" + currMap.user_id + "/" + currMap.name + ".txt";

        FileInfo file = new FileInfo(filePath);
        file.Directory.Create();

        File.WriteAllText(filePath, jsonGenerated);
    }
}
