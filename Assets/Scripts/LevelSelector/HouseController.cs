﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class HouseController : MonoBehaviour {
    public int id;
    public string levelToLoad;
    public List<float> performances;
	public GameObject previewMini;
	public float offsetY;
    public float scalePreviewMini;
    public string pretty_best_time_string;
    public string last_feedback;
    public string pretty_name;

    
    public void UpdateScoresCommunity(Scores scores)
    {
        if (scores != null)
        {
            performances.Clear();
            float bestTime = -1;
            foreach (var score in scores.scores)
            {
                performances.Add(score.ComputeNote());
                var currTime = score.timeSpentResolving;

                if ((currTime > 0 && (bestTime == -1 || currTime < bestTime)))
                    bestTime = currTime;
            }
            if (bestTime >= 0.01)
                pretty_best_time_string = "Meilleur Temps: " + Environment.NewLine + TimeUtils.PrettyPrintTime(bestTime);

            last_feedback = scores.scores[scores.scores.Count - 1].ComputeFeedBack();
        }
        else
        {
            pretty_best_time_string = "";
            last_feedback = "";
        }
    }
}
