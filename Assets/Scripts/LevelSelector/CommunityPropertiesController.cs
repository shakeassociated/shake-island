﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CommunityPropertiesController : MonoBehaviour {
    Text mapName, author;

    private void Awake()
    {
        mapName = transform.Find("Map_Name").GetComponent<Text>();
        author = transform.Find("Author").GetComponent<Text>();
    }

    public void SetMap(Map map)
    {
        mapName.text = map.name;
        author.text = "par " + map.user.pseudo;
    }

    public void Clear()
    {
        mapName.text = "";
        author.text = "";
    }
}
