﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModalController : MonoBehaviour {
    Text titleText, messageText;
    bool inGame;
    public bool InGame
    {
        get
        {
            return inGame;
        }
    }

    private void Awake()
    {
        titleText = transform.FindDeepChild("Title").GetComponent<Text>();
        messageText = transform.FindDeepChild("Message").GetComponent<Text>();
    }

    public void Set(string title, string message, bool inGame)
    {
        titleText.text = title;
        messageText.text = message;
        this.inGame = inGame;
    }
}
