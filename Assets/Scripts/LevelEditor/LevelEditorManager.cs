﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MaterialChangerData
{
    private string name;
    public string Name
    {
        get { return name; }
    }

    private List<string> names;
    public List<string> Names
    {
        get { return names; }
    }

    private int index;
    public int Index
    {
        get { return index; }
    }

    private GameObject materialChanger;
    public GameObject MaterialChanger
    {
        get
        {
            return materialChanger;
        }
    }

    public MaterialChangerData(TextureController tc)
    {
        name = tc.name;

        names = new List<string>();
        foreach (var m in tc.materials)
            names.Add(m.name);

        index = 0;
    }

    public void ChangeMaterial(bool next)
    {
        if (next)
        {
            index++;
            if (index == names.Count)
                index = 0;
        }
        else
        {
            index--;
            if (index < 0)
                index = names.Count - 1;
        }

        Update();
    }

    public void SetMaterialChanger(GameObject o)
    {
        materialChanger = o;
        Update();
    }

    public void Update()
    {
        materialChanger.transform.Find("Name").GetComponent<Text>().text = name;
        materialChanger.transform.Find("Texture").GetComponent<Text>().text = names[index];
    }

    public override string ToString()
    {
        var res = "Material changer : " + name + Environment.NewLine;
        foreach (var s in names)
            res += s + " ";
        res += Environment.NewLine + "Index : " + index;
        return res;
    }
}

public class LevelEditorManager : MonoBehaviour
{
    public string currPrefabName;

    GameController gameController;
    PlayerController playerController;
    HUDController hudCtrl;

    public GameObject leftContentGameObject, textureContentGameObject, textureGameObject, collectablesPropertiesGameObject,
        npcPropertiesGameObject, modalPrefab;
    public Button add_button;
    public Text object_name;

    GameObject currPrefab, currPanel;
    CollectableController currCollectableController;
    NPCController currNPCController;

    // Properties
    public GameObject objectInListPrefab;
    public enum ObjectTypeToDisplay { COLLECTABLES, FURNITURES, NPC, NONE };
    ObjectTypeToDisplay currObjectTypeToDisplay;
    string talkTextNPC;
    InputField talkTextAreaNPCInputField;
    Toggle canFinishToggle, canTalkToggle;
    public AuthController authController;

    List<MaterialChangerData> materialChangerList;
    public GameObject materialChangerPrefab;

    private void Awake()
    {
        currPrefabName = "";
        playerController = FindObjectOfType<PlayerController>();
        gameController = playerController.gameController;
        hudCtrl = FindObjectOfType<HUDController>();

        materialChangerList = new List<MaterialChangerData>();

        talkTextAreaNPCInputField = npcPropertiesGameObject.transform.Find("TalkText").GetComponent<InputField>();
        canFinishToggle = npcPropertiesGameObject.transform.Find("CanFinish").GetComponent<Toggle>();
        canTalkToggle = npcPropertiesGameObject.transform.Find("CanTalk").GetComponent<Toggle>();
    }

    private void Start()
    {
        add_button.interactable = false;
        object_name.text = "";
        currPrefab = null;
        currPanel = null;
        currCollectableController = null;
        currNPCController = null;
        currObjectTypeToDisplay = ObjectTypeToDisplay.NONE;
        collectablesPropertiesGameObject.SetActive(false);
        textureGameObject.SetActive(false);
        npcPropertiesGameObject.SetActive(false);
        talkTextNPC = "";
    }

    private void FillMaterialChangerList()
    {
        var tc = currPrefab.GetComponent<TextureController>();
        if (tc)
            materialChangerList.Add(new MaterialChangerData(tc));
        FillMaterialChangerListRec(currPrefab.transform);
    }

    private void FillMaterialChangerListRec(Transform o)
    {
        foreach (Transform child in o.transform)
        {
            var tc = child.GetComponent<TextureController>();
            if (tc)
                materialChangerList.Add(new MaterialChangerData(tc));

            FillMaterialChangerListRec(child);
        }
    }

    public void ChangeCurrPrefab(GameObject panel, string newPrefabName, string newPrettyName)
    {
        // Clear
        foreach (var mc in materialChangerList)
            DestroyImmediate(mc.MaterialChanger);

        if (currPanel != null)
            currPanel.GetComponent<Image>().enabled = false;
        currPanel = panel;
        if (currPanel != null)
            currPanel.GetComponent<Image>().enabled = true;

        materialChangerList.Clear();
        collectablesPropertiesGameObject.SetActive(false);
        textureGameObject.SetActive(false);
        npcPropertiesGameObject.SetActive(false);
        currNPCController = null;
        talkTextNPC = "";
        canFinishToggle.isOn = true;
        canTalkToggle.isOn = true;
        talkTextAreaNPCInputField.text = "";

        // Creation
        currPrefabName = newPrefabName;
        add_button.interactable = newPrettyName != "";
        object_name.text = newPrettyName;

        if (newPrefabName == "")
        {
            currPrefab = null;
            currCollectableController = null;
        }
        else
        {
            currPrefab = gameController.GetPrefab(currPrefabName);
            currCollectableController = currPrefab.GetComponent<CollectableController>();

            if (currCollectableController.IsCollectableType() || currCollectableController.IsEditorFurnitureType())
            {
                textureGameObject.SetActive(true);
                FillMaterialChangerList();

                for (int i = 0; i < materialChangerList.Count; i++)
                {
                    GameObject newMc = Instantiate(materialChangerPrefab);
                    newMc.transform.SetParent(textureContentGameObject.transform);

                    newMc.GetComponent<RectTransform>().anchoredPosition = materialChangerPrefab.GetComponent<RectTransform>().anchoredPosition;
                    newMc.GetComponent<RectTransform>().localPosition += new Vector3(0, -i * 55, 0);

                    var currMc = materialChangerList[i];
                    newMc.transform.Find("ButtonLeft").GetComponent<Button>().onClick.AddListener(delegate { SetTexture(currMc.Name, false); });
                    newMc.transform.Find("ButtonRight").GetComponent<Button>().onClick.AddListener(delegate { SetTexture(currMc.Name, true); });
                    currMc.SetMaterialChanger(newMc);
                }
            }
            else // NPC
            {
                npcPropertiesGameObject.SetActive(true);
                currNPCController = currPrefab.GetComponent<NPCController>();
                currNPCController.buttonFinish = true;
                currNPCController.buttonTalk = true;
            }

            // Enable auditive for collectables
            if (currCollectableController.IsCollectableType())
                collectablesPropertiesGameObject.SetActive(true);
        }
    }

    public void CollectFromPrefab()
    {
        GameObject o = Instantiate(currPrefab);
        o.name = currPrefab.name;

        foreach (var mc in materialChangerList)
        {
            var materialObj = o.transform.FindDeepChild(mc.Name);
            materialObj.GetComponent<TextureController>().currentMaterialIndex = mc.Index;
        }

        ObjectController objController = o.GetComponent<ObjectController>();
        objController.IncreaseID(); // Setting id !

        var colController = o.GetComponent<CollectableController>();
        colController.auditive = currCollectableController.auditive;

        // Setting inner slots' ids and layer
        if (colController.IsEditorType())
        {
            foreach (Transform child in o.transform)
            {
                if (child.gameObject.GetComponent<SlotController>() != null)
                    child.GetComponent<ObjectController>().IncreaseID();
            }
        }

        if (currNPCController != null) // npc
        {
            var npcController = o.GetComponent<NPCController>();
            npcController.buttonFinish = currNPCController.buttonFinish;

            npcController.buttonTalk = canTalkToggle.isOn && talkTextAreaNPCInputField.text != "";
            npcController.npcButtonTalkController.fullText = talkTextAreaNPCInputField.text;
        }
        colController.Take();
        playerController.Collect(o);
        hudCtrl.DisplayCollectedObject(o);
        gameController.EnableSlotsColliders(!colController.IsEditorType());
        if (colController.IsEditorType())
            gameController.ChangeLayersToIgnore(true);
    }

    private ObjectTypeToDisplay StringToObjectType(string s)
    {
        ObjectTypeToDisplay res;
        switch (s)
        {
            case "COLLECTABLES":
                res = ObjectTypeToDisplay.COLLECTABLES;
                break;
            case "NPC":
                res = ObjectTypeToDisplay.NPC;
                break;
            case "FURNITURES":
                res = ObjectTypeToDisplay.FURNITURES;
                break;
            default:
                res = ObjectTypeToDisplay.NONE;
                break;
        }
        return res;
    }

    private bool IsObjectOfType(GameObject o, ObjectTypeToDisplay objType)
    {
        var res = false;
        var collectableCtrl = o.GetComponent<CollectableController>();

        if (!collectableCtrl)
            return false;
        switch (objType)
        {
            case ObjectTypeToDisplay.COLLECTABLES:
                res = collectableCtrl.IsCollectableType();
                break;
            case ObjectTypeToDisplay.FURNITURES:
                res = collectableCtrl.IsEditorFurnitureType();
                break;
            case ObjectTypeToDisplay.NPC:
                res = collectableCtrl.collectableType == CollectableType.NPC_EDITOR;
                break;
        }

        return res;
    }

    public void UpdateObjectsList(string s)
    {
        foreach (Transform child in leftContentGameObject.transform)
            Destroy(child.gameObject);

        currObjectTypeToDisplay = StringToObjectType(s);

        List<GameObject> objects = new List<GameObject>();

        if (currObjectTypeToDisplay != ObjectTypeToDisplay.NONE)
        {
            foreach (var prefab in gameController.prefabs)
            {
                // Separates all kinds of objects (furnitures and collectables) via their controller
                if (IsObjectOfType(prefab, currObjectTypeToDisplay))
                    objects.Add(prefab);
            }
        }

        objects.Sort(delegate (GameObject a, GameObject b)
        {
            return (a.GetComponent<ObjectController>().prettyName).CompareTo(b.GetComponent<ObjectController>().prettyName);
        });

        for (int i = 0; i < objects.Count; i++)
        {
            GameObject o = Instantiate(objectInListPrefab);
            o.transform.SetParent(leftContentGameObject.transform);

            var rectTransform = o.GetComponent<RectTransform>();
            var rectTransformPrefab = objectInListPrefab.GetComponent<RectTransform>();

            rectTransform.anchoredPosition = rectTransformPrefab.anchoredPosition;

            var newLocalPosition = new Vector3(
                rectTransform.localPosition.x,
                rectTransform.localPosition.y - i * 26,
                0);
            rectTransform.localPosition = newLocalPosition;
            rectTransform.localScale = new Vector3(1, 1, 1);

            var currPrefabName = objects[i].name;
            var currPrettyName = objects[i].GetComponent<ObjectController>().prettyName;

            o.transform.Find("Text").GetComponent<Text>().text = currPrettyName;

            var trigger = o.GetComponent<EventTrigger>();
            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerClick;
            entry.callback.AddListener((eventData) => { ChangeCurrPrefab(o, currPrefabName, currPrettyName); });
            trigger.triggers.Add(entry);
        }

        // Disable previous selected object
        ChangeCurrPrefab(null, "", "");
    }

    public void SetTexture(string materialChangerName, bool next)
    {
        MaterialChangerData mc = null;
        foreach (var n in materialChangerList)
        {
            if (n.Name == materialChangerName)
                mc = n;
        }
        if (mc != null)
            mc.ChangeMaterial(next);
    }

    public void SetCollectableAuditive(bool val)
    {
        currCollectableController.auditive = val;
    }

    public void ChangeCanFinishNPC(bool val)
    {
        if (currNPCController != null)
            currNPCController.buttonFinish = val;
    }

    public void ChangeCanTalkNPC(bool val)
    {
        talkTextAreaNPCInputField.interactable = val;
    }

    public bool SaveMap(bool withModal)
    {
        bool atLeastOneCollectable = false;

        bool atLeastOneFinishingNPC = false;
        bool canSave = false;
        foreach (var col in GameObject.FindObjectsOfType<CollectableController>())
        {
            if (col.IsCollectableType())
                atLeastOneCollectable = true;

            if (col.IsNPC())
            {
                var npcCtrl = col.GetComponent<NPCController>();
                if (npcCtrl.buttonFinish)
                    atLeastOneFinishingNPC = true;
            }

            if ((canSave = atLeastOneFinishingNPC && atLeastOneCollectable))
                break;
        }

        if (canSave)
        {
            gameController.SaveMap();
            if (withModal)
                gameController.hudController.CreateModal("Sauvegarde", "La carte a bien été sauvegardée !", false);
        }
        else
            hudCtrl.CreateModal("Sauvegarde", "Il doit y avoir au moins " + 
                (atLeastOneCollectable ? "un personnage qui finit" : "un collectable") + " sur la carte.", false);

        return canSave;
    }

    public void Save()
    {
        SaveMap(true);
    }

    public void PublishMap()
    {
        if (!authController.IsConnected())
            authController.Open("CHOOSE");
        else
        {
            if (SaveMap(false))
            {
                StartCoroutine(authController.mapsController.PublishMap(
                    gameController.mapController.mapInfo,
                    (res, error) =>
                    { 
                        if (res)
                            hudCtrl.CreateModal("Publication", "La carte a bien été publiée !", false);
                        else
                        {
                            hudCtrl.CreateModal("Publication", "La carte n'a pas pu etre publiée. Reconnectez-vous.", false);
                            authController.ClearUserData();
                        }
                    }
                    ));
            }
        }
    }
}
