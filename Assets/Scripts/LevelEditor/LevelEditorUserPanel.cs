﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelEditorUserPanel : MonoBehaviour {
    bool isConnected;
    GameObject connectedPanel, disconnectedPanel;
    Text helloUser;

    private void Awake()
    {
        connectedPanel = transform.Find("IsConnected").gameObject;
        disconnectedPanel = transform.Find("IsDisconnected").gameObject;
        helloUser = connectedPanel.transform.Find("Text").GetComponent<Text>();
    }

    private void Start()
    {
        isConnected = CheckIsConnected();
        connectedPanel.SetActive(isConnected);
        disconnectedPanel.SetActive(!isConnected);
        if (isConnected)
            helloUser.text = "Bonjour " + PlayerPrefs.GetString("pseudo") + "!";
    }

    void Update ()
    {
        var newIsConnected = CheckIsConnected();

		if (isConnected && !newIsConnected)
        {
            connectedPanel.SetActive(false);
            disconnectedPanel.SetActive(true);
            isConnected = false;
        }
        else if (!isConnected && newIsConnected)
        {
            connectedPanel.SetActive(true);
            disconnectedPanel.SetActive(false);
            helloUser.text = "Bonjour " + PlayerPrefs.GetString("pseudo") + "!";
            isConnected = true;
        }
	}

    private bool CheckIsConnected()
    {
        return PlayerPrefs.HasKey("jwt") && PlayerPrefs.HasKey("pseudo");
    }
}
