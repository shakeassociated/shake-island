﻿public class TimeUtils {
    public static string PrettyPrintTime(float duration)
    {
        int minutes = (int)duration / 60;
        int secondes = (int)duration % 60;

        var res = "";
        res += (minutes < 10 ? "0" + minutes.ToString() : minutes.ToString());
        res += ":" + (secondes < 10 ? "0" + secondes.ToString() : secondes.ToString());

        return res;
    }
}
