﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotateAround : MonoBehaviour {
    public Transform t;
    Vector3 position;

    private void Start()
    {
        position = t.position;
    }

    void Update () {
        transform.LookAt(position);
        transform.RotateAround(position, Vector3.up, 200 * Time.deltaTime);
    }
}
