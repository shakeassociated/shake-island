﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAnimator : MonoBehaviour {
    public float min, max;

	void Start () {
        if (min == 0)
            min = 0.4f;
        if (max == 0)
            max = 1.6f;

        var animator = GetComponent<Animator>().speed = Random.Range(min, max);
	}
}
