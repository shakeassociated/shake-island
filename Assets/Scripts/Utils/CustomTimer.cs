﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public class CustomTimer
    {
        float duration, currDuration;

        public CustomTimer(float duration)
        {
            this.duration = duration;
            currDuration = 0f;
        }

        public void Restart()
        {
            currDuration = duration;
        }

        public void Update()
        {
            if (!IsOver())
                currDuration -= Time.deltaTime;
        }

        public void Stop()
        {
            currDuration = 0;
        }

        public bool IsOver()
        {
            return currDuration <= 0;
        }
    }
}
