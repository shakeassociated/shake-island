﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CreateMeshColliders : Editor
{
    [MenuItem("Custom/Mesh Colliders/Create Mesh Colliders")]
    private static void AddMeshColliders()
    {
        foreach (GameObject o in Selection.objects)
        {
            var gameObjectSet = o.GetComponentsInChildren<Transform>();

            foreach (var item in gameObjectSet)
            {
                if (item.GetComponent<MeshRenderer>() != null && item.GetComponent<MeshCollider>() == null)
                {
                    item.gameObject.AddComponent<MeshCollider>();
                }
            }
        }
    }

    [MenuItem("Custom/Mesh Colliders/Set To Convex")]
    private static void SetToConvex()
    {
        foreach (GameObject o in Selection.objects)
        {
            var gameObjectSet = o.GetComponentsInChildren<Transform>();

            foreach (var item in gameObjectSet)
            {
                var mc = item.GetComponent<MeshCollider>();
                if (mc != null)
                {
                    mc.convex = true;

                    foreach (Transform t in item)
                    {
                        var mc2 = t.GetComponent<MeshCollider>();
                        if (mc2 != null)
                            mc2.convex = true;
                    }
                }
            }
        }
    }
}
