﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ApplyObjectBorderShader : Editor {

    [MenuItem("Custom/Apply ObjectBorder Shader")]
    private static void ApplyObjectBorderShaderToSelection()
    {
        Material yourMaterial = (Material)Resources.Load("ObjectBorder", typeof(Material));
        Debug.Log("Correctly found " + yourMaterial.name);

        var gameObjectSet = Selection.activeGameObject.GetComponentsInChildren<Transform>();

        foreach (var item in gameObjectSet)
        {
            foreach (Transform o in item)
            {
                var renderer = o.GetComponent<Renderer>();
                if (renderer != null)
                {
                    var material = renderer.sharedMaterial;
                }
            }
        }
    }
}
