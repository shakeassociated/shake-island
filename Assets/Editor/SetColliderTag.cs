﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SetColliderTag : Editor {
    [MenuItem("Custom/Set Collider Tag")]
    private static void SetTagToCollider()
    {
        foreach (GameObject o in Selection.objects)
        {
            o.tag = "collider";
            foreach (Transform t in o.transform)
            {
                t.tag = "collider";
                foreach (Transform tt in t)
                {
                    tt.tag = "collider";
                    foreach (Transform ttt in tt)
                        ttt.tag = "collider";
                }

            }
        }
    }
}
