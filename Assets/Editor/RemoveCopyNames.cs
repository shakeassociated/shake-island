﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class RemoveCopyNames : Editor
{
    [MenuItem("Custom/RemoveCopyNames")]
    private static void RemoveCopyNamesOfSelection()
    {
        var objects = Selection.objects;

        foreach (var o in objects)
        {
            var index = o.name.IndexOf(" (");
            if (index >= 0)
                o.name = o.name.Remove(index);
        }
    }
}
