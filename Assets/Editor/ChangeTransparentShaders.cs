﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ChangeTransparentShaders : Editor
{

    //[MenuItem("Custom/Change Shaders To Transparent")]
    private static void MakeTransparent()
    {
        Shader transparent = Shader.Find("Transparent/Diffuse");

        var meshesRenderer = Object.FindObjectsOfType<MeshRenderer>();
        foreach (var mesh in meshesRenderer)
        {
            foreach (Material mat in mesh.sharedMaterials)
            {
                if (mat.shader != null)
                {
                    mat.shader = transparent;
                }
            }
        }
    }

    //[MenuItem("Custom/Change Shaders To Standard")]
    private static void MakeStandard()
    {
        Shader standard = Shader.Find("Standard");

        var meshesRenderer = Object.FindObjectsOfType<MeshRenderer>();
        foreach (var mesh in meshesRenderer)
        {
            foreach (Material material in mesh.sharedMaterials)
            {
                if (material.shader != null)
                {
                    material.shader = standard;
                }
            }
        }
    }

}
