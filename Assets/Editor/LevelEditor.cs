﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class LevelEditor : Editor
{
    [MenuItem("Custom/Level Editor/Transform basic object to collectable")]
    private static void BasicToCollectable()
    {
        EditorApplication.ExecuteMenuItem("Custom/Mesh Colliders/Create Mesh Colliders");
        EditorApplication.ExecuteMenuItem("Custom/Mesh Colliders/Set To Convex");
        EditorApplication.ExecuteMenuItem("Custom/Set Collider Tag");

        foreach (GameObject o in Selection.objects)
        {
            if (o.GetComponent<ObjectController>() == null)
                o.AddComponent<ObjectController>();
            o.AddComponent<Rigidbody>();
            o.AddComponent<CollectableController>();
            o.GetComponent<CollectableController>().collectableType = CollectableType.FURNITURE_EDITOR;
        }
    }

    [MenuItem("Custom/Level Editor/Transform basic object to collectable (No MeshCollider)")]
    private static void BasicToCollectableNoMeshCollider()
    {
        EditorApplication.ExecuteMenuItem("Custom/Set Collider Tag");

        foreach (GameObject o in Selection.objects)
        {
            if (o.GetComponent<ObjectController>() == null)
                o.AddComponent<ObjectController>();
            o.AddComponent<Rigidbody>();
            o.AddComponent<CollectableController>();
            o.GetComponent<CollectableController>().collectableType = CollectableType.FURNITURE_EDITOR;
        }
    }
}
