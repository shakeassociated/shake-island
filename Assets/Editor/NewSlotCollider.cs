﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class NewSlotCollider : Editor
{
    [MenuItem("Custom/Slots/New collider")]
    private static void SlotNewCollider()
    {
        foreach (GameObject item in Selection.objects)
        {
            if (item.GetComponent<SlotController>() != null)
            {
                if (item.GetComponent<BoxCollider>() == null)
                {
                    var bc = item.gameObject.AddComponent<BoxCollider>();
                    bc.center = new Vector3(0, 5.04f, 0);
                    bc.size = new Vector3(10, 10, 10);
                }
            }
        }
    }
}
